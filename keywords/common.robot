*** Keywords ***
Generate random number
    [Arguments]    ${len}
    ${random_number}    Generate Random String    ${len}    chars=[NUMBERS]
    [Return]    ${random_number}

Generate random text
    [Arguments]    ${len}    ${option}
    ${random_string}    Generate Random String    ${len}    chars=[${option}]
    [Return]    ${random_string}

Generate status date
    [Arguments]    ${increment}
    ${status_date}    DateTime.Get current date    increment=${increment}    result_format=%Y-%m-%d %H:%M    exclude_millis=True
    [Return]    ${status_date}

Generate shipping order date
    [Arguments]    ${increment}
    ${increment}  BuiltIn.Evaluate  ${increment}+${UTC_timezone}
    ${order_date}  DateTime.Get current date    time_zone=UTC    increment=${increment} hours    result_format=%Y-%m-%d %H:%M:%S   exclude_millis=True
    RETURN    ${order_date}

Generate Date
    [Arguments]    ${increment}
    ${date}    DateTime.Get current date    increment=${increment}    result_format=%Y-%m-%d    exclude_millis=True
    [Return]    ${date}

Get currect date time by format UTC
    ${current_time}    DateTime.Get current date    result_format=%Y-%m-%dT%H:%M:%SZ    time_zone=UTC
    RETURN     ${current_time}

Get currect date time
    ${current_time}    DateTime.Get current date      time_zone=UTC
    ${current_time}    DateTime.Add time to date      ${current_time}   ${UTC_timezone} hours       result_format=%Y-%m-%d %H:%M      exclude_millis=True
    RETURN     ${current_time}

Get currect time and add more
    [Arguments]    ${add_time}
    ${current_time}      common.Get currect date time
    ${current_time}      DateTime.Add time to date      ${current_time}   ${add_time}    result_format=%Y-%m-%d %H:%M     exclude_millis=True
    RETURN     ${current_time}
