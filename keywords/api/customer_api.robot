*** Keywords ***
Post create customer
    [Documentation]    ...
    [Arguments]      ${customer_data}
    ${create_customer_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/create_customer_template.json
    ${update_customer_json}    JSONLibrary.Update value to json    ${create_customer_template}    $.companyTaxId         ${customer_data.customer_tax_id}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.companyRegisteredName    ${customer_data.customer_regis_name}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.companyNameTH            ${customer_data.customer_name_th}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.companyNameEN            ${customer_data.customer_name_en}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.companyPhoneNumber       ${customer_data.customer_phone}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.customerSAPCode          ${customer_data.customer_sap_code}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.statusStartDate          ${customer_data.customer_start_date}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.keyContactVos[0].keyContactPhoneNumber    8${customer_data.customer_phone}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.keyContactVos[0].keyContactTitle          ${customer_data.customer_title}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.keyContactVos[0].keyContactFirstName      ${customer_data.customer_firstname}
    ${update_customer_json}    JSONLibrary.Update value to json    ${update_customer_json}    $.keyContactVos[0].keyContactLastName       ${customer_data.customer_lastname}
    ${final_body}              JSONLibrary.Update value to json    ${update_customer_json}    $.keyContactVos[0].keyContactEmail          ${customer_data.customer_mail}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}      RequestsLibrary.POST on session    tms_session    ${api_config.customer_add}    json=${final_body}     expected_status=200
    BuiltIn.Log       ${response.content}
    [Return]    ${response.json()['items']}

Get customer detail
    [Documentation]    ...
    [Arguments]    ${login_cp_ticket_dev}    ${customer_item_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${customer_item_id}    BuiltIn.Convert to string    ${customer_item_id}
    ${api_config.customer_detail}    String.Replace string    ${api_config.customer_detail}    xxxxxxxxx    ${customer_item_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.customer_detail}
    BuiltIn.Log    ${response.content}
