*** Keywords ***
Create shipping order as order confirmed
    [Documentation]    Create shipping order and confirm
    [Arguments]     ${order_data}
    order_api_feature.Prepare data for create the order    ${order_data}
    ${count}  BuiltIn.Get length   ${list_site_detail}
    FOR    ${i}  IN RANGE  0   ${count}
        ${site_info_id}   site_api.Get site info ID     ${list_site_detail[${i}]}
        Collections.Set to dictionary    ${list_site_detail[${i}]}    site_info_id=${site_info_id}
    END
    ${list_PID}=    BuiltIn.Create list
    FOR    ${data}  IN   @{list_date_data}
        shipping_order_api.Post create confirm shipping order   ${data}     ${list_site_detail}
        ${pid}  deliver_plan_api.Get PID for create dispatch order
        ${pid}  Convert to string     ${pid}
        Collections.Append to list    ${list_PID}    ${pid}
    END
    RETURN    ${list_PID}

Get shipping order and save
    [Documentation]    save shipping order
    [Arguments]     ${date}     ${customer_name}
    ${count}  BuiltIn.Get length   ${list_site_detail}
    FOR    ${i}  IN RANGE  0   ${count}
        ${site_info_id}   site_api.Get site info ID     ${list_site_detail[${i}]}
        Collections.Set to dictionary    ${list_site_detail[${i}]}    site_info_id=${site_info_id}
    END
    shipping_order_api.Post create save shipping order  ${date}   ${list_site_detail}
    ${order_no}    shipping_order_api.Get latest shipping order no from customer name     ${customer_name}
    BuiltIn.Set test variable    ${order_no}    ${order_no}
    RETURN    ${order_no}

Create dispatch order and confirm dispatch order
    [Documentation]    Get shipping order to create dispatch order
    [Arguments]    ${list_shipping_order}
    ${pid_list}    BuiltIn.Evaluate   ",".join(${list_shipping_order})
    ${route_id}    deliver_plan_api.Create dispatch order    ${pid_list}
    deliver_plan_api.Confirm dispatch order       ${route_id}
    BuiltIn.Set test variable    ${dispatch_order_id}    ${route_id}

Create shipping order and assign the job to driver
    [Documentation]  Create the job and assign the driver
    [Arguments]     ${order_data}
    order_api_feature.Prepare data for create the order       ${order_data}
    ${list_shipping_order}    order_api_feature.Get shipping order and confirm the order       ${list_date_data}
    ${order}      shipping_order_api.Get order no from customer name         ${customer_name}
    ${order_detail}    order_api_feature.Prepare order data after confirm shipping order          ${list_site_detail}     ${order}
    order_api_feature.Create dispatch order and confirm dispatch order       ${list_shipping_order}
    schedule_api.Assign driver and confirm dispatch
    ${token}    login_api.Get token from login driver via API
    notification_api.Send API to accept job       ${token}
    RETURN    ${order_detail}

Get shipping order and confirm the order
    [Arguments]     ${list_date_data}
    ${count}  BuiltIn.Get length   ${list_site_detail}
    FOR    ${i}  IN RANGE  0   ${count}
        ${site_info_id}   site_api.Get site info ID     ${list_site_detail[${i}]}
        Collections.Set to dictionary    ${list_site_detail[${i}]}    site_info_id=${site_info_id}
    END
    ${list_shipping_order}=    BuiltIn.Create list
    ${count}  BuiltIn.Get length   ${list_date_data}
    FOR    ${i}  IN RANGE  0   ${count}
        shipping_order_api.Post create confirm shipping order       ${list_date_data[${i}]}     ${list_site_detail}
        ${pid}    deliver_plan_api.Get PID for create dispatch order
        ${pid}    Convert to string     ${pid}
        Collections.Set to dictionary    ${list_date_data[${i}]}    pid=${pid}
        Collections.Append to list    ${list_shipping_order}    ${pid}
    END
    RETURN    ${list_shipping_order}

Created shipping order
    [Documentation]    Create shipping order and save
    [Arguments]     ${order_data}
    order_api_feature.Prepare data for create the order     ${order_data}
    ${list_of_order}=    BuiltIn.Create list
    FOR    ${date}  IN   @{list_date_data}
        ${order_no}      order_api_feature.Get shipping order and save      ${date}      ${customer_name}
        Collections.Append to list   ${list_of_order}    ${order_no}
    END
    RETURN    ${list_of_order}

Prepare data for create the order
    [Arguments]     ${order_data}
    master_data_api_feature.Create customer via API
    master_data_api_feature.Create site via API
    # master_data_api_feature.Create sub-contractor via API
    # master_data_api_feature.Create driver via API
    # master_data_api_feature.Create vehicle via API
    ${list_date_data}=    BuiltIn.Create list
    FOR    ${data}  IN   @{order_data}
        ${earlier_deliver_date}    common.Generate shipping order date   ${data['hour_earlier_pickup']}
        ${latest_deliver_date}     common.Generate shipping order date   ${data['hour_latest_pickup']}
        ${earlier_receive_date}    common.Generate shipping order date   ${data['hour_earlier_delivery']}
        ${latest_receive_date}     common.Generate shipping order date   ${data['hour_latest_delivery']}
        &{data_date}=    BuiltIn.Create dictionary
        ...    earlier_deliver_date=${earlier_deliver_date}
        ...    latest_deliver_date=${latest_deliver_date}
        ...    earlier_receive_date=${earlier_receive_date}
        ...    latest_receive_date=${latest_receive_date}
        ...    total_weight=${data['total_weight']}
        ...    total_num_items_per_packages=${data['total_num_items_per_packages']}
        ...    volume=${data['cubic_meter']}
        ...    packing_qty=${data['packing_qty']}
        Collections.Append to list    ${list_date_data}    ${data_date}
    END
    BuiltIn.Set test variable    ${list_date_data}    ${list_date_data}

Prepare order data after confirm shipping order
    [Arguments]         ${list_site_detail}     ${order}
    FOR    ${data}  IN   @{list_date_data}
        Collections.Set to dictionary    ${data}    pickup_site_code=${list_site_detail[0].site_code}
        Collections.Set to dictionary    ${data}    pickup_site_name=${list_site_detail[0].site_names}
        Collections.Set to dictionary    ${data}    delivery_site_code=${list_site_detail[1].site_code}
        Collections.Set to dictionary    ${data}    delivery_site_name=${list_site_detail[1].site_names}
    END
    ${count}  BuiltIn.Get length   ${order}
    FOR    ${i}  IN RANGE  0   ${count}
        ${b}      BuiltIn.Evaluate    ${i}+1
        ${y}      BuiltIn.Evaluate    ${count}-${b}
        ${shipping_no}   shipping_order_api.Get shipping no from order no       ${order}[${y}][orderNo]
        Collections.Set to dictionary    ${list_date_data[${i}]}    order_no=${order}[${y}][orderNo]
        Collections.Set to dictionary    ${list_date_data[${i}]}    shipping_no=${shipping_no}
    END
    RETURN    ${list_date_data}
