*** Keywords ***
Check tracking status by index
    [Documentation]    ...
    [Arguments]    ${status_list}    ${index}    ${expected_status}
    ${checking_status}    BuiltIn.Set Variable    ${status_list}[${index}]
    ${status}      Collections.Get From Dictionary     ${checking_status}    status
    BuiltIn.Should Be Equal As Strings    ${status}    ${expected_status}

Check latest status
    [Documentation]    ...
    [Arguments]    ${value}    ${expected}
    BuiltIn.Should Be Equal As Strings    ${value}    ${expected}