*** Keywords ***
Create customer via API
    [Documentation]    ...
    ${customer_start_date}      common.Generate status date    0
    ${customer_tax_id}          common.Generate random number    13
    ${customer_regis_name}      BuiltIn.Set variable    ${customer.customer_regis_name}${customer_tax_id}
    ${customer_phone}           common.Generate random number    8
    ${customer_name_th}         String.Replace string    ${customer.customer_name_th}     xx        ${customer_regis_name}
    ${customer_name_en}         String.Replace string    ${customer.customer_name_en}     xx        ${customer_regis_name}
    ${customer_sap_code}        BuiltIn.Set variable    ${customer.customer_sap_code}${customer_tax_id}
    &{data}=    BuiltIn.Create dictionary
    ...    customer_start_date=${customer_start_date}
    ...    customer_tax_id=${customer_tax_id}
    ...    customer_regis_name=${customer_regis_name}
    ...    customer_phone=${customer_phone}
    ...    customer_name_th=${customer_name_th}
    ...    customer_name_en=${customer_name_en}
    ...    customer_sap_code=${customer_sap_code}
    ...    customer_title=${customer.customer_title}
    ...    customer_firstname=${customer.customer_firstname}
    ...    customer_lastname=${customer.customer_lastname}
    ...    customer_mail=${customer.customer_mail}
    ${return_items}    customer_api.Post create customer     ${data}
    BuiltIn.Set test variable     ${customer_id}             ${return_items}
    BuiltIn.Set test variable     ${customer_name}           ${customer_regis_name}
    BuiltIn.Set test variable     ${customer_phone}          ${customer_phone}
    BuiltIn.Set test variable     ${customer_start_date}     ${customer_start_date}

Create site via API
    [Documentation]    ...
    ${count}  BuiltIn.Get length   ${sites.site_detail}
    ${list_site_detail}=    BuiltIn.Create list
    FOR    ${i}  IN RANGE  0   ${count}
        ${random_name}    common.Generate random text      5    LETTERS
        ${random_code}    common.Generate random number    15
        ${site_names}     BuiltIn.Set variable      ${sites.site_name}-${customer_name}${random_name}
        Collections.Set to dictionary    ${sites.site_detail[${i}]}    site_code=${random_code}
        Collections.Set to dictionary    ${sites.site_detail[${i}]}    site_names=${site_names}
        Collections.Set to dictionary    ${sites.site_detail[${i}]}    site_phone=${customer_phone}
        Collections.Set to dictionary    ${sites.site_detail[${i}]}    site_start_date=${customer_start_date}
        ${return_items}   site_api.Post create site        ${sites.site_detail[${i}]}
        Collections.Set to dictionary    ${sites.site_detail[${i}]}    site_id=${return_items}
        Collections.Append to list    ${list_site_detail}    ${sites.site_detail[${i}]}
    END
    BuiltIn.Set test variable     ${list_site_detail}    ${list_site_detail}

Create sub-contractor via API
    [Documentation]    ...
    ${sub_contractor_tax_id}        common.Generate random number    13
    ${sub_contractor_phone}         common.Generate random number    8
    ${sub_contractor_bank_no}       common.Generate random number    10
    ${sub_contractor_regis_name}    BuiltIn.Set variable      ${sub_contractor.sub_contractor_name}${sub_contractor_tax_id}
    ${sub_contractor_name_th}       String.Replace string    ${sub_contractor.sub_contractor_name_th}     xx        ${sub_contractor_regis_name}
    ${sub_contractor_name_en}       String.Replace string    ${sub_contractor.sub_contractor_name_en}     xx        ${sub_contractor_regis_name}
    ${sub_contractor_bank_name}     BuiltIn.Set variable      ${sub_contractor_name_th}
    ${sub_contractor_sap_code}      BuiltIn.Set variable      ${sub_contractor.sub_contractor_sap_code}${sub_contractor_tax_id}
    &{data}=    BuiltIn.Create dictionary
    ...    sub_contractor_tax_id=${sub_contractor_tax_id}
    ...    sub_contractor_regis_name=${sub_contractor_regis_name}
    ...    sub_contractor_phone=${sub_contractor_phone}
    ...    sub_contractor_bank_no=${sub_contractor_bank_no}
    ...    sub_contractor_name_th=${sub_contractor_name_th}
    ...    sub_contractor_name_en=${sub_contractor_name_en}
    ...    sub_contractor_sap_code=${sub_contractor_sap_code}
    ...    sub_contractor_bank_name=${sub_contractor_bank_name}
    ...    sub_contractor_start_date=${customer_start_date}
    ${return_items}    subcontractor_api.Post create sub contractor  ${data}
    ${s_id}    subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}    ${return_items}
    BuiltIn.Set test variable    ${sub_contractor_text_id}    ${return_items}
    BuiltIn.Set test variable    ${sub_contractor_id}    ${s_id}
    BuiltIn.Set test variable    ${sub_contractor_regis_name}    ${sub_contractor_regis_name}

Create driver via API
    [Documentation]    ...
    ${random_string}    common.Generate random text    5    LETTERS
    ${driver_phone}     common.Generate random number    9
    ${driver_id_no}     common.Generate random number    13
    ${driver_license_id}    common.Generate random number    16
    ${driver_name}    BuiltIn.Set variable    ${drivers.driver_name}${random_string}
    ${s_id}      subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}      ${sub_contractor_text_id}
    BuiltIn.Set test variable    ${sub_contractor_id}    ${s_id}
    &{data}=    BuiltIn.Create dictionary
    ...    driver_phone=${driver_phone}
    ...    driver_id_no=${driver_id_no}
    ...    driver_license_id=${driver_license_id}
    ...    driver_name=${driver_name}
    ...    driver_start_date=${customer_start_date}
    ...    sub_contractor_id=${sub_contractor_id}
    ${return_items}       driver_api.Post create driver    ${data}
    ${dateOfBirth}        String.Remove string         ${drivers.dateOfBirth}     -
    ${driver_password}    Convert to string     ${dateOfBirth}
    BuiltIn.Set test variable    ${driver_id}    ${return_items}
    BuiltIn.Set test variable    ${driver_license_id}    ${driver_license_id}
    BuiltIn.Set test variable    ${driver_name}    ${driver_name}
    BuiltIn.Set test variable    ${driver_phone}    ${driver_phone}
    BuiltIn.Set test variable    ${driver_password}    ${driver_password}

Create vehicle via API
    [Documentation]    ...
    ${random_string}    common.Generate random text    3    LETTERS
    ${random_number}    common.Generate random number    4
    ${license_no}    BuiltIn.Set variable    8${random_string}-${random_number}
    ${s_id}    subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}       ${sub_contractor_text_id}
    BuiltIn.Set test variable       ${sub_contractor_id}    ${s_id}
    vehicle_api.Post create vehicle     ${license_no}
    BuiltIn.Set test variable    ${license_no}    ${license_no}
