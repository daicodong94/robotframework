*** Keywords ***
Post create vehicle
    [Documentation]
    [Arguments]     ${license_no}
    ${create_vehicle_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/create_vehicle_template.json
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${create_vehicle_template}    $.licensePlateNumber    ${license_no}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.vehicleType               ${vehicle.vehicle_type}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.serviceGroup              ${vehicle.vehicle_group}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.vehicleInsurance          ${vehicle.insurance}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.provincialSign            ${vehicle.provincal_sign}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.listDrivers[0].id         ${driver_id}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.tmsSubcontractorId        ${sub_contractor_id}
    ${update_vehicle_json}    JSONLibrary.Update value to json    ${update_vehicle_json}    $.tmsSiteId                 ${list_site_detail[0].site_id}
    ${final_body}             JSONLibrary.Update value to json    ${update_vehicle_json}    $.siteCode                  ${list_site_detail[0].site_code}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.POST on session    tms_session    ${api_config.vehicle_add}   json=${final_body}
    BuiltIn.Log    ${response.content}
