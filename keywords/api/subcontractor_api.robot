*** Keywords ***
Post create sub contractor
    [Documentation]
    [Arguments]    ${data}
    ${create_subcontractor_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/create_subcontractor_template.json
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${create_subcontractor_template}    $.companyTaxId         ${data.sub_contractor_tax_id}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.companyRegisteredName    ${data.sub_contractor_regis_name}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.companyNameTH            ${data.sub_contractor_name_th}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.companyNameEN            ${data.sub_contractor_name_en}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.companyPhoneNumber       ${data.sub_contractor_phone}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.vendorSAPCode            ${data.sub_contractor_sap_code}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.bankAccountNumber        ${data.sub_contractor_bank_no}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.bankAccountName          ${data.sub_contractor_bank_name}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.bankInformationVo.bankAccountNumber       ${data.sub_contractor_bank_no}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.bankInformationVo.bankAccountName         ${data.sub_contractor_bank_name}
    ${update_subcontractor_json}    JSONLibrary.Update value to json    ${update_subcontractor_json}    $.statusStartDate                           ${data.sub_contractor_start_date}
    ${final_body}                   JSONLibrary.Update value to json    ${update_subcontractor_json}    $.keyContactVos[0].keyContactPhoneNumber    8${data.sub_contractor_phone}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}      RequestsLibrary.POST on session    tms_session    ${api_config.subcontractor_add}    json=${final_body}        expected_status=200
    BuiltIn.Log       ${response.content}
    [Return]    ${response.json()['items']}

Get sub contractor detail
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${sub_contractor_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${sub_contractor_id}    BuiltIn.Convert to string    ${sub_contractor_id}
    ${api_config.subcontractor_detail}    String.Replace string    ${api_config.subcontractor_detail}    xxxxxxxxx    ${sub_contractor_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.subcontractor_detail}
    BuiltIn.Log       ${response.content}
    ${id}    BuiltIn.Set variable    ${response.json()['items']['id']}
    BuiltIn.Log    sub contractor : ${id}
    [Return]    ${id}