*** Keywords ***
Send API to accept job
    [Arguments]      ${token}
    ${current_dateTime}     common.Get currect date time by format UTC
    ${message_id}           database.Get message id from DB by job no
    ${body}          JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/mobile/${env}/accept_new_job.json
    ${body}          JSONLibrary.Update value to json    ${body}    $.jobNo           ${job_no}
    ${body}          JSONLibrary.Update value to json    ${body}    $.ackDateTime     ${current_dateTime}
    ${final_body}    JSONLibrary.Update value to json    ${body}    $.msgID           ${message_id}
    ${header}       BuiltIn.Create dictionary    Authorization=${token}
    ${response}     RequestsLibrary.POST on session    tms_session      ${api_config.notification_accept}    headers=${header}    json=${final_body}    expected_status=200
