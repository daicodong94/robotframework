*** Keywords ***
Send and update shipment to on the way
    [Arguments]      ${order_detail}
    FOR    ${data}  IN   @{order_detail}
        ${current_time}      common.Get currect date time
        ${list_shipping_status}=    BuiltIn.Create list

        &{arrival_pickup}    BuiltIn.Create dictionary
        ...    status=${shipping_status.pickup.arrival_pickup}
        ...    time=${current_time}

        &{dock_assigned}    BuiltIn.Create dictionary
        ...    status=${shipping_status.pickup.dock_assigned}
        ...    time=${current_time}

        &{loading}    BuiltIn.Create dictionary
        ...    status=${shipping_status.pickup.loading}
        ...    time=${current_time}

        &{ontheway}    BuiltIn.Create dictionary
        ...    status=${shipping_status.on_the_way.on_the_way}
        ...    weight=${data.total_weight}
        ...    actualQuantityOfGoods=${data.total_num_items_per_packages}
        ...    actualQuantityOfPackages=${data.packing_qty}
        ...    volume=${data.volume}
        ...    temperature=${good_pickup_temperature}
        ...    uploadImage=
        ...    time=${current_time}
        Collections.Append to list    ${list_shipping_status}    ${arrival_pickup}
        Collections.Append to list    ${list_shipping_status}    ${dock_assigned}
        Collections.Append to list    ${list_shipping_status}    ${loading}
        Collections.Append to list    ${list_shipping_status}    ${ontheway}
        ${body}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/${env}/update_shipment_status.json
        ${body}    JSONLibrary.Update value to json    ${body}    $.pId                     ${data.pid}
        ${body}    JSONLibrary.Update value to json    ${body}    $.listShipmentStatus      ${list_shipping_status}
        ${body}    JSONLibrary.Update value to json    ${body}    $.expectStatus            ${shipping_status.on_the_way.on_the_way}
        ${final_body}    JSONLibrary.Update value to json    ${body}    $.currentStatus     ${shipping_status.new.dispatched}
        common.Create TMS Session    ${login_cp_ticket_dev}
        ${response}     RequestsLibrary.POST on session    tms_session    ${api_config.shipment_status}       json=${final_body}     expected_status=200
    END

Send and update shipment to delivered
    [Arguments]      ${order_detail}     ${default_added_time}=60s
    FOR    ${data}  IN   @{order_detail}
        ${current_time}     common.Get currect time and add more    ${default_added_time}
        ${list_shipping_status}=    BuiltIn.Create list

        &{arrival_destination}    BuiltIn.Create dictionary
        ...    status=${shipping_status.delivery.arrival_destination}
        ...    time=${current_time}

        &{dock_assigned}    BuiltIn.Create dictionary
        ...    status=${shipping_status.delivery.dock_assigned}
        ...    time=${current_time}

        &{unloading}    BuiltIn.Create dictionary
        ...    status=${shipping_status.delivery.unloading}
        ...    time=${current_time}

        &{deliverey}    BuiltIn.Create dictionary
        ...    status=${shipping_status.delivery.delivered}
        ...    damagedQuantity=
        ...    damagedPackage=
        ...    actualQuantityOfGoods=${data.total_num_items_per_packages}
        ...    actualQuantityOfPackages=${data.packing_qty}
        ...    rejectedQuantity=
        ...    rejectedPackage=
        ...    time=${current_time}
        ...    temperature=${good_delivered_temperature}
        Collections.Append to list    ${list_shipping_status}    ${arrival_destination}
        Collections.Append to list    ${list_shipping_status}    ${dock_assigned}
        Collections.Append to list    ${list_shipping_status}    ${unloading}
        Collections.Append to list    ${list_shipping_status}    ${deliverey}
        ${body}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/${env}/update_shipment_status.json
        ${body}    JSONLibrary.Update value to json    ${body}    $.pId                     ${data.pid}
        ${body}    JSONLibrary.Update value to json    ${body}    $.listShipmentStatus      ${list_shipping_status}
        ${body}    JSONLibrary.Update value to json    ${body}    $.expectStatus            ${shipping_status.delivery.delivered}
        ${final_body}    JSONLibrary.Update value to json    ${body}    $.currentStatus     ${shipping_status.on_the_way.on_the_way}
        common.Create TMS Session    ${login_cp_ticket_dev}
        ${response}     RequestsLibrary.POST on session    tms_session    ${api_config.shipment_status}       json=${final_body}     expected_status=200
    END

Send and update shipment to pickup fail
    [Arguments]      ${order_detail}        ${default_added_time}=60s
    FOR    ${data}  IN   @{order_detail}
        ${current_time}      common.Get currect date time
        ${time_pickup_fail}      common.Get currect time and add more    ${default_added_time}
        ${list_shipping_status}=    BuiltIn.Create list

        &{arrival_pickup}    BuiltIn.Create dictionary
        ...    status=${shipping_status.pickup.arrival_pickup}
        ...    time=${current_time}

        &{pick_up_failed}    BuiltIn.Create dictionary
        ...    status=${shipping_status.pickup.pick_up_failed}
        ...    reason=make pickup fail
        ...    cancelled=2   #default
        ...    time=${time_pickup_fail}

        Collections.Append to list    ${list_shipping_status}    ${arrival_pickup}
        Collections.Append to list    ${list_shipping_status}    ${pick_up_failed}
        ${body}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/${env}/update_shipment_status.json
        ${body}    JSONLibrary.Update value to json    ${body}    $.pId                     ${data.pid}
        ${body}    JSONLibrary.Update value to json    ${body}    $.listShipmentStatus      ${list_shipping_status}
        ${body}    JSONLibrary.Update value to json    ${body}    $.expectStatus            ${shipping_status.pickup.pick_up_failed}
        ${final_body}    JSONLibrary.Update value to json    ${body}    $.currentStatus     ${shipping_status.new.dispatched}
        common.Create TMS Session    ${login_cp_ticket_dev}
        ${response}     RequestsLibrary.POST on session    tms_session    ${api_config.shipment_status}       json=${final_body}     expected_status=200
    END