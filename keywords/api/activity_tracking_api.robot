*** Keywords ***
Get list job tracking status
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${job_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_job}    params=id=${job_id}
    BuiltIn.Log    ${response.content}
    ${list_job_tracking_status}    BuiltIn.Set Variable    ${response.json()['items']['trackingStatusList']}
    Collections.Log List    ${list_job_tracking_status}
    [Return]    ${list_job_tracking_status}

Get current job status
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${job_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}      RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_job}    params=id=${job_id}
    BuiltIn.Log      ${response.content}
    ${job_status}    BuiltIn.Set Variable    ${response.json()['items']['status']}
    BuiltIn.Log      ${job_status}
    [Return]    ${job_status}

Get list shippment tracking status
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${shipment_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${shipment_id}    BuiltIn.Convert to string    ${shipment_id}
    ${api_config.activity_detail_shipment}    String.Replace string    ${api_config.activity_detail_shipment}    xxxxxxxxx    ${shipment_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_shipment}
    BuiltIn.Log    ${response.content}
    ${list_shipment_tracking_status}    BuiltIn.Set Variable    ${response.json()['items']['shipmentStatusVoList']}
    Collections.Log List    ${list_shipment_tracking_status}
    [Return]    ${list_shipment_tracking_status}

​​​Get current shipment status
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${shipment_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${api_config.activity_detail_shipment}    String.Replace string    ${api_config.activity_detail_shipment}    xxxxxxxxx    ${shipment_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_shipment}
    BuiltIn.Log    ${response.content}
    ${shipment_status}    BuiltIn.Set Variable    ${response.json()['items']['status']}
    BuiltIn.Log    ${shipment_status}
    [Return]    ${shipment_status}

Get order id from shipment id
    [Documentation]    ...
    [Arguments]    ${login_cp_ticket_dev}    ${shipment_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${shipment_id}    BuiltIn.Convert to string    ${shipment_id}
    ${api_config.activity_detail_shipment}    String.Replace string    ${api_config.activity_detail_shipment}    xxxxxxxxx    ${shipment_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_shipment}
    BuiltIn.Log    ${response.content}
    ${order_id}    BuiltIn.Set Variable    ${response.json()['items']['parentOrderId']}
    BuiltIn.Log    ${order_id}
    [Return]    ${order_id}

Get order no from shipment id
    [Documentation]    ...
    [Arguments]    ${login_cp_ticket_dev}    ${shipment_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${shipment_id}    BuiltIn.Convert to string    ${shipment_id}
    ${api_config.activity_detail_shipment}    String.Replace string    ${api_config.activity_detail_shipment}    xxxxxxxxx    ${shipment_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_shipment}
    BuiltIn.Log    ${response.content}
    ${order_no}    BuiltIn.Set Variable    ${response.json()['items']['parentOrderNumber']}
    BuiltIn.Log    ${order_no}
    [Return]    ${order_no}

Get list order tracking status
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${order_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${order_id}    BuiltIn.Convert to string    ${order_id}
    ${api_config.activity_detail_order}    String.Replace string    ${api_config.activity_detail_order}    xxxxxxxxx    ${order_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_order}
    BuiltIn.Log    ${response.content}
    ${list_order_tracking_status}    BuiltIn.Set Variable    ${response.json()['items']['orderStatusVoList']}
    Collections.Log List    ${list_order_tracking_status}
    [Return]    ${list_order_tracking_status}

​​​Get current order status
    [Documentation]
    [Arguments]    ${login_cp_ticket_dev}    ${order_id}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${order_id}    BuiltIn.Convert to string    ${order_id}
    ${api_config.activity_detail_order}    String.Replace string    ${api_config.activity_detail_order}    xxxxxxxxx    ${order_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.activity_detail_order}
    BuiltIn.Log    ${response.content}
    ${order_status}     BuiltIn.Set Variable    ${response.json()['items']['orderStatus']}
    BuiltIn.Log    ${order_status}
    [Return]    ${order_status}
