*** Keywords ***
Create TMS Login Session
    [Documentation]    ....
    RequestsLibrary.Create session    tms_login_session    ${SERVER_CONFIG.SUPPLIER_URL}    verify=True

Post Login To TMS Web
    [Documentation]
    [Arguments]     ${username}    ${password}    ${rememberFlag}=false
    ${login_body}   BuiltIn.Set Variable    {"username": "${username}","password": "${password}","rememberMe": ${rememberFlag}}
    ${json_body}    JSONLibrary.Convert String To Json    ${login_body}
    common.Create TMS Login Session
    ${response}     RequestsLibrary.POST on session    tms_login_session    ${api_config.tms_login}    json=${json_body}
    BuiltIn.Log    ${response.content}
    ${login_cp_ticket_dev}    BuiltIn.Set Variable    ${response.json()['items']['login_cp_ticket_dev']}
    [Return]    ${login_cp_ticket_dev}

Get token from login driver via API
    ${body}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/mobile/${env}/login.json
    ${body}    JSONLibrary.Update value to json    ${body}    $.phone     ${driver_phoneno}
    ${final_body}    JSONLibrary.Update value to json    ${body}    $.password     ${driver_passwords}
    RequestsLibrary.Create session      account-api       ${SERVER_CONFIG.ACCOUNT_URL}     verify=true
    ${response}     RequestsLibrary.POST on session    account-api      ${api_config.driver_login}       params=_t=1669606332094     json=${final_body}    expected_status=200
    [Return]   ${response.json()['items']['token']['access_token']}