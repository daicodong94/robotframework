*** Keywords ***
Post create confirm shipping order
    [Documentation]    ...
    [Arguments]  ${data}     ${site_detail}
    ${create_shipping_order_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/create_shipping_order_template.json
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${create_shipping_order_template}    $.partnerName    ${customer_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.partnerId    ${customer_id}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.originatingSiteName    ${site_detail[0].site_names}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.originatingSiteCode    ${site_detail[0].site_code}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.originatingSiteId      ${site_detail[0].site_info_id}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderMobile           668${site_detail[0].site_phone}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.sender                 ${site_detail[0].site_firstname} ${site_detail[0].site_lastname}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderAddress          ${site_detail[0].address}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderProvince         ${site_detail[0].province_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderCity             ${site_detail[0].district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderDistrict         ${site_detail[0].sub_district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.weight                 ${data.total_weight}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.goodsQty               ${data.total_num_items_per_packages}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.volume                 ${data.volume}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.packingQty             ${data.packing_qty}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverMobile         668${site_detail[1].site_phone}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiver               ${site_detail[1].site_firstname} ${site_detail[0].site_lastname}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverAddress        ${site_detail[1].address}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.destinationSiteName    ${site_detail[1].site_names}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.destinationSiteCode    ${site_detail[1].site_code}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.destinationSiteId      ${site_detail[1].site_info_id}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverProvince       ${site_detail[1].province_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverCity           ${site_detail[1].district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverDistrict       ${site_detail[1].sub_district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.earliestDeliveryDt    ${data.earlier_deliver_date}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.latestDeliveryDt    ${data.latest_deliver_date}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.earliestReceivedDt    ${data.earlier_receive_date}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.latestReceivedDt    ${data.latest_receive_date}
    ${final_body}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.goodsBillNo    ${customer_name}
    BuiltIn.Log    ${final_body}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.POST on session    tms_session    ${api_config.order_confirm}    json=${final_body}      expected_status=200
    BuiltIn.Log    ${response.content}

Post create save shipping order
    [Documentation]    ...
    [Arguments]    ${data}     ${site_detail}
    ${create_shipping_order_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/create_shipping_order_template.json
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${create_shipping_order_template}    $.partnerName        ${customer_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.partnerId              ${customer_id}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.originatingSiteName    ${site_detail[0].site_names}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.originatingSiteCode    ${site_detail[0].site_code}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.originatingSiteId      ${site_detail[0].site_info_id}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderMobile           668${site_detail[0].site_phone}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.sender                 ${site_detail[0].site_firstname} ${site_detail[0].site_lastname}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderAddress          ${site_detail[0].address}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderProvince         ${site_detail[0].province_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderCity             ${site_detail[0].district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.senderDistrict         ${site_detail[0].sub_district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverMobile         668${site_detail[1].site_phone}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiver               ${site_detail[1].site_firstname} ${site_detail[0].site_lastname}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverAddress        ${site_detail[1].address}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.destinationSiteName    ${site_detail[1].site_names}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.destinationSiteCode    ${site_detail[1].site_code}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.destinationSiteId      ${site_detail[1].site_info_id}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverProvince       ${site_detail[1].province_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverCity           ${site_detail[1].district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.receiverDistrict       ${site_detail[1].sub_district_name}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.earliestDeliveryDt     ${data.earlier_deliver_date}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.latestDeliveryDt       ${data.latest_deliver_date}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.earliestReceivedDt     ${data.earlier_receive_date}
    ${update_shipping_order_json}    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.latestReceivedDt       ${data.latest_receive_date}
    ${final_body}                    JSONLibrary.Update value to json    ${update_shipping_order_json}    $.goodsBillNo            ${customer_name}
    BuiltIn.Log    ${final_body}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.POST on session    tms_session    ${api_config.order_api}    json=${final_body}      expected_status=200
    BuiltIn.Log    ${response.content}

Get latest shipping order id from customer name
    [Documentation]    ...
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.shipping_order_find}    params=?partnerName=${customer_name}&orderStatus=order_created     expected_status=200
    BuiltIn.Log    ${response.content}
    [Return]    ${response.json()['items']['list'][0]['pid']}

Get latest shipping order no from customer name
    [Documentation]    Get order no while order status order_created
    [Arguments]      ${customer_name}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.shipping_order_find}    params=?partnerName=${customer_name}&orderStatus=order_created      expected_status=200
    BuiltIn.Log       ${response.content}
    [Return]    ${response.json()['items']['list'][0]['orderNo']}

Get order no from customer name
    [Documentation]    Able to get order no before order_confirm status
    [Arguments]      ${customer_name}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.shipping_order_find}    params=partnerName=${customer_name}     expected_status=200
    log to console      ${response.json()}
    [Return]    ${response.json()['items']['list']}

Get shipping no from order no
    [Documentation]    Able to get shipping no before order_confirm status
    [Arguments]      ${order_no}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.shipping_order_search}    params=q=${order_no}&s=SHIPMENT_CREATED&type=transport     expected_status=200
    [Return]       ${response.json()['items']['items'][0]['transport_code']}

Get latest customer SAP code from customer name
    [Arguments]    ${customer_name}
    common.Create TMS session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.shipping_order_find}    params=?partnerName=${customer_name}&orderStatus=order_created      expected_status=200
    [Return]    ${response.json()['items']['list'][0]['goodsBillNo']}
