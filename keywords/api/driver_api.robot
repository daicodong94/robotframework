*** Keywords ***
Post create driver
    [Documentation]    ...
    [Arguments]      ${data}
    ${create_driver_template}    Catenate    {'driverPhotoFileName': (None,''), 'driverPhotoLink': (None,''),
    ...    'driverNameTitle': (None,'1'), 'driverFirstName': (None, '${data.driver_name}'), 'driverLastName': (None,'${drivers.driver_lastname}'),
    ...    'gender': (None,'3'), 'dateOfBirth': (None,'1996-05-02'),
    ...    'emailOne': (None, ''), 'emailTwo': (None,''),
    ...    'mobileCountryCode': (None,'66'), 'contactNumber': (None,'${data.driver_phone}'),
    ...    'landlineCountryCode': (None,'66'), 'landlinePhoneNo': (None,''), 'landlinePhoneExtension': (None,''),
    ...    'driverNationalIdPhotoFileName': (None,''), 'driverNationalIdPhotoLink': (None,''),
    ...    'nationalIdCardNumber': (None,'${data.driver_id_no}'), 'nationalIdCardExpiredDate': (None,'2025-05-01'),
    ...    'drivingLicensePhotoFileName': (None,''), 'drivingLicensePhotoLink': (None,''),
    ...    'driverLicenseNumber': (None,'${data.driver_license_id}'), 'driverLicenseType': (None,'1'), 'driverLicenseCategory': (None,'4'),
    ...    'driverLicenseIssueDate': (None,'2020-03-05'), 'driverLicenseExpiredDate': (None,'2025-03-04'),
    ...    'statusType': (None,'1'), 'statusStartDate': (None,'${data.driver_start_date}'), 'statusEndDate': (None,''),
    ...    'tmsSubContractorId': (None,'${data.sub_contractor_id}'), 'vehicleId': (None,''),
    ...    'isPrevNationalPhotoExist': (None,'false'), 'isPrevLicensePhotoExist': (None,'false')
    ...    }
    ${driver_body}    BuiltIn.Evaluate    ${create_driver_template}
    BuiltIn.Log    ${driver_body}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}       RequestsLibrary.POST on session    tms_session    ${api_config.driver_add}      files=${driver_body}      expected_status=200
    BuiltIn.Log    ${response.content}
    ${driver_id}      BuiltIn.Convert to integer    ${response.content}
    RETURN    ${driver_id}
