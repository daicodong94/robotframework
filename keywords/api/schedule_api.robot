*** Keywords ***
Get schedule info
    [Documentation]    ...
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.query_by_id}    params=pid=${dispatch_order_id}
    BuiltIn.Log       ${response.content}
    [Return]    ${response}

Get partner ID
    [Documentation]    ...
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.partners_search}    params=q=${sub_contractor_regis_name}&t=c&s=1&pageNo=1&pageSize=1
    BuiltIn.Log       ${response.content}
    ${partner_id}    BuiltIn.Convert to integer     ${response.json()['items']['items'][0]['e_id']}
    [Return]    ${partner_id}

Get transport order ID
    [Documentation]    ...
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${dispatch_order_id}    BuiltIn.Convert to string    ${dispatch_order_id}
    ${api_config.transportorders_id}    String.Replace string    ${api_config.transportorders_id}    xxxxxxxxx    ${dispatch_order_id}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.transportorders_id}
    BuiltIn.Log       ${response.content}
    [Return]    ${response.json()['items'][0]['pid']}

Assign driver and confirm dispatch
    [Documentation]    ...
    ${route_info}    schedule_api.Get schedule info
    ${partner_id}    schedule_api.Get partner ID
    ${transport_order_id}    schedule_api.Get transport order ID
    ${create_new_order_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/confirm_add_new_order_template.json
    ${update_new_order_json}    JSONLibrary.Update value to json    ${create_new_order_template}    $.id    ${route_info.json()['items']['id']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.routeNo    ${route_info.json()['items']['routeNo']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.originatingSiteId    ${route_info.json()['items']['originatingSiteId']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.originatingSiteName    ${route_info.json()['items']['originatingSiteName']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.partnerId    ${partner_id}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.editedDt    ${route_info.json()['items']['editedDt']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.earliestBeginTime    ${route_info.json()['items']['earliestBeginTime']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.latestBeginTime    ${route_info.json()['items']['latestBeginTime']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.earliestEndTime    ${route_info.json()['items']['earliestEndTime']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.latestEndTime    ${route_info.json()['items']['latestEndTime']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.partnerName    ${sub_contractor_regis_name}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.routeName    ${route_info.json()['items']['routeName']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.planBeginDt    ${route_info.json()['items']['planBeginDt']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.planEndDt    ${route_info.json()['items']['planEndDt']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.pid    ${route_info.json()['items']['pid']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.routeId    ${route_info.json()['items']['pid']}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.transportOrderIds[0]    ${transport_order_id}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.capacityList[0].driver    ${driver_name} ${driver_lastname}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.capacityList[0].mobile   66${driver_phoneno}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.capacityList[0].plateNo     ${license_no}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.capacityList[0].identification    ${driver_license_id}
    ${update_new_order_json}    JSONLibrary.Update value to json    ${update_new_order_json}    $.capacityList[0].partnerId    ${partner_id}
    ${final_body}    JSONLibrary.Update value to json    ${update_new_order_json}    $.capacityList[0].partnerName    ${sub_contractor_regis_name}
    common.Create TMS Session    ${login_cp_ticket_dev}
    BuiltIn.Set test variable    ${job_no}     ${route_info.json()['items']['routeNo']}
    ${response}      RequestsLibrary.POST on session    tms_session    ${api_config.route_save}   params=confirm=true    json=${final_body}      expected_status=200
    BuiltIn.Log    ${response.content}
    BuiltIn.Log    Job no:${job_no}
