*** Keywords ***
Get Sites List
    [Documentation]    ...
    [Arguments]    ${login_cp_ticket_dev}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.site_list}
    BuiltIn.Log    ${response.content}

Post create site
    [Documentation]    ...
    [Arguments]   ${data}
    ${create_site_template}    JSONLibrary.Load json from file    ${CURDIR}/../../resources/testdata/web/staging/create_site_template.json
    ${update_site_json}    JSONLibrary.Update value to json    ${create_site_template}    $.customerId          ${customer_id}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.siteCode            ${data.site_code}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.siteName            ${data.site_names}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.siteTypeId          ${data.site_type}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.siteSubTypeId       ${data.site_sub_typeId}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.address             ${data.address}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.provinceId          ${data.province_Id}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.districtId          ${data.district_Id}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.subDistrictId       ${data.sub_district_Id}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.postalCode          ${data.postal_code}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.lat                 ${data.latitude}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.lng                 ${data.longitude}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.geofencingRange     ${data.geofencing_range}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.status.startDate    ${data.site_start_date}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.keyContactVos[0].keyContactTitle        ${data.site_title}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.keyContactVos[0].keyContactFirstName    ${data.site_firstname}
    ${update_site_json}    JSONLibrary.Update value to json    ${update_site_json}        $.keyContactVos[0].keyContactLastName     ${data.site_lastname}
    ${final_body}          JSONLibrary.Update value to json    ${update_site_json}        $.keyContactVos[0].keyContactPhoneNumber  8${data.site_phone}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.POST on session    tms_session    ${api_config.site_add}    json=${final_body}       expected_status=200
    BuiltIn.Log       ${response.content}
    [Return]    ${response.json()['items']}

Get site info ID
    [Documentation]    ...
    [Arguments]     ${site_detail}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.site_list}    params=searchText=${site_detail.site_names}
    BuiltIn.Log       ${response.content}
    [Return]    ${response.json()['items']['list'][0]['siteInfoId']}