*** Keywords ***
Create TMS Session
    [Arguments]    ${login_cp_ticket_dev}
    ${header}      BuiltIn.Create Dictionary    Cookie=login_cp_ticket_dev="${login_cp_ticket_dev}"
    RequestsLibrary.Create session    tms_session    ${SERVER_CONFIG.TMS_URL}    headers=${header}   verify=True

Set Up TMS Web Login Token
    [Documentation]    ...
    [Arguments]    ${user}    ${password}
    ${token}       login_api.Post Login To TMS Web    ${user}    ${password}
    BuiltIn.Set Global Variable    ${login_cp_ticket_dev}    ${token}