*** Keywords ***
Get PID for create dispatch order
    [Documentation]    ...
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.Get on session    tms_session    ${api_config.route_query}    params=partnerName=${customer_name}
    BuiltIn.Log    ${response.content}
    [Return]    ${response.json()['items']['list'][0]['pid']}

Create dispatch order
    [Documentation]    ...
    [Arguments]      ${pids}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}     RequestsLibrary.POST on session    tms_session    ${api_config.route_create}     params=pids=${pids}
    BuiltIn.Log    ${response.content}
    [Return]    ${response.json()['items']}

Confirm dispatch order
    [Documentation]    ...
    [Arguments]      ${pids}
    common.Create TMS Session    ${login_cp_ticket_dev}
    ${response}    RequestsLibrary.POST on session    tms_session    ${api_config.route_confirm}   params=pids=${pids}&vehicleTypeCode=VTH1&requiredTypeCode=true
    BuiltIn.Log    ${response.content}
