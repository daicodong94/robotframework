*** Keywords ***
Search keyword and check result
    customer_list_page.Input search keyword on search box  ${TC_master_data_customer['customer_id']}
    customer_list_page.Click search button
    customer_list_page.Check search result

Filter with customer type
    customer_list_page.Click filter button
    customer_list_page.Click dropdown customer type
    customer_list_page.Click select customer type in dropdown list  ${master_data['customer_personal']}

Check reset filter button and filter result
    customer_list_page.Check reset filter button is appeared
    customer_list_page.Check filter result