*** Keywords ***
Login to TMS website
    [Arguments]    ${email}         ${password}
    web_login_page.Input email TMS website          ${email}
    web_login_page.Input password TMS website       ${password}
    web_login_page.Click login button
    home_feature.Select language on home page

Login to TMS website with remember me
    [Arguments]    ${email}         ${password}
    web_login_page.Input email TMS website          ${email}
    web_login_page.Input password TMS website       ${password}
    web_login_page.Click remember me checkbox
    web_login_page.Click login button
    home_feature.Select language on home page

Login to TMS website and get cookie
    [Arguments]    ${email}    ${password}    ${browser}=chrome
    login_feature.Login to TMS website    ${email}    ${password}
    ${cookie}    SeleniumLibrary.Get Cookie      login_cp_ticket_dev
    ${token}    String.Remove string    ${cookie.value}    "
    BuiltIn.Set suite variable    ${login_cp_ticket_dev}    ${token}

Login to TMS website with remember me and get cookie
    [Arguments]    ${email}    ${password}    ${browser}=chrome
    login_feature.Login to TMS website with remember me    ${email}    ${password}
    ${cookie}    SeleniumLibrary.Get Cookie      login_cp_ticket_dev
    ${token}    String.Remove string    ${cookie.value}    "
    BuiltIn.Set suite variable    ${login_cp_ticket_dev}    ${token}