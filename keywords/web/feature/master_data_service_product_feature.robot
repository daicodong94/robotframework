*** Keywords ***
Add new service product
    [Documentation]    ...
    [Arguments]    ${service_product_page.txt_service_product_name}      ${service_product_page.txt_service_product_code}    ${service_product_page.txt_service_short_name}    ${service_product_page.txt_service_ratio}
    master_data_service_product_page.Input service product name          ${service_product_page.txt_service_product_name}
    master_data_service_product_page.Input service product code          ${service_product_page.txt_service_product_code}
    master_data_service_product_page.Click to select project attribute
    master_data_service_product_page.Input service product short name    ${service_product_page.txt_service_short_name}
    master_data_service_product_page.Input service product ratio         ${service_product_page.txt_service_ratio}
    master_data_service_product_page.Click save to create a new service product