*** Keywords ***
Select language on home page
    [Documentation]     Change language on home page by using language variable
    ${default_lang_en}=      home_page.Verify default language is english
        IF    '${LANG}' == 'en'
            IF      '${default_lang_en}' == 'False'
                common.Click element when ready     ${home.img_change_language}
                common.Click element when ready     ${home.btn_en_lang}
            END
        ELSE IF     '${LANG}' == 'th'
            IF      '${default_lang_en}' == 'True'
                common.Click element when ready     ${home.img_change_language}
                common.Click element when ready     ${home.btn_th_lang}
            END
        END