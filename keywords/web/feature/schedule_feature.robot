*** Keywords ***
Input dispatch number with order number then search
    [Arguments]     ${order_no}
    schedule_page.Input dispatch number into textbox    ${order_no}
    schedule_page.Click to select highlight dispatch number result     ${order_no}
    schedule_page.Click inquire button

Add new order for accepted jobs
    [Arguments]     ${waybill_number}
    schedule_page.Click add new order after select dispatch number
    schedule_page.Fill into waybill number textbox          ${waybill_number}
    schedule_page.Select add new order searching result     ${waybill_number}