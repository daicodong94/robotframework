*** Keywords ***
Select waybill list by using order number
    [Arguments]     ${order_number}
    ${waybill_number}=   delivery_plan_page.Get waybill number by using order number     ${order_number}
    delivery_plan_page.Select a desire waybill number list      ${waybill_number}

Click create dispatch order and check dialog popup
    delivery_plan_page.Click create a dispatch order button
    delivery_plan_page.Check if dialog popup displayed when create a dispatch order

Searching dispatch number by using waybill number
    [Arguments]     ${waybill_number}
    delivery_plan_page.Input dispatch number into textbox       ${waybill_number}
    ${dispatch_number}=     delivery_plan_page.Get dispatch number from searching result
    delivery_plan_page.Click to select highlight dispatch number result     ${waybill_number}
    delivery_plan_page.Click inquire button
    [Return]    ${dispatch_number}

Find needed column in plan page
    [Arguments]    ${index}    ${number_of_columns}    ${column_name}    ${value}
    ${index}    BuiltIn.Convert to string    ${index}
    ${locator}    String.Replace string    ${lbl_column_name}    ***index***    ${index}
    ${found_column}    BuiltIn.Run keyword and return status    SeleniumLibrary.Element text should be    ${locator}    ${column_name}
    IF    ${found_column}
        shipping_order_feature.Verify that value field of table is correct    ${index}    ${value}
    END
    BuiltIn.Run keyword if    ${index}==${number_of_columns}    BuiltIn.Should be true    ${found_column}
    [Return]    ${found_column}

Get specific number columns of table plan page
    [Arguments]    ${column_name}    ${value}
    ${number_of_columns}    shipping_order_list_page.Get number columns of table
    FOR    ${index}    IN RANGE    1    ${number_of_columns}+1
        ${found_column}    delivery_plan_feature.Find needed column in plan page    ${index}    ${number_of_columns}    ${column_name}    ${value}
        BuiltIn.Exit for loop if    ${found_column}
    END
