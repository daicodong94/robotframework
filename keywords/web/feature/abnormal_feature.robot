*** Keywords ***
Check exception type of order is
    [Arguments]      ${exception_type}
    ${number_of_columns}   abnormal_page.Get number columns of table
    FOR    ${row}    IN RANGE    1    ${number_of_columns}+1
        ${row}        BuiltIn.Convert to string    ${row}
        ${locator}    String.Replace string    ${abnormal.lbl_column_exception_type}    ***index***    ${row}
        ${found_column}    BuiltIn.Run keyword and return status    SeleniumLibrary.Element text should be   ${locator}    ${exception_type}
        BuiltIn.Exit for loop if    ${found_column}
    END
    BuiltIn.Should be true      ${found_column}
    RETURN    ${row}

Order should be correct with data
    [Arguments]      ${exception_type}    ${abnormal_status}       ${source_status}
    ${row}     abnormal_feature.Check exception type of order is        ${exception_type}
    abnormal_page.Check abnormal status of order with refer to row      ${abnormal_status}     ${row}
    abnormal_page.Check source status of order with refer to row        ${source_status}     ${row}