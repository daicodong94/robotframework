*** Keywords ***
Go to shipping order page
    SeleniumLibrary.Go to    ${SERVER_CONFIG.TMS_URL}/order/entry

Search by shipping order number
    [Documentation]    ...
    [Arguments]    ${shipping_order_no}
    shipping_order_list_page.Click more filter shipping order
    shipping_order_list_page.Input filter by shipping order number      ${shipping_order_no}
    shipping_order_list_page.Click button inquiry shipping order

Search by customer sap code
    [Arguments]    ${customer_sap_code}
    shipping_order_list_page.Click more filter shipping order
    shipping_order_list_page.Click customer SAP code
    shipping_order_list_page.Input value on filter in shipping order page    ${customer_sap_code}
    shipping_order_list_page.Select filter by customer SAP code    ${customer_sap_code}
    shipping_order_list_page.Click button inquiry shipping order

Revise Customer
    [Documentation]    ...
    [Arguments]    ${customer_name}
    shipping_order_list_page.Click revise customer
    shipping_order_create_page.Input dropdown search    ${customer_name}
    shipping_order_create_page.Select item from list by index   ${checkbox_index['first_index']}

Revise origin site
    [Documentation]    ...
    [Arguments]    ${site_name}
    shipping_order_list_page.Click revise origin site
    shipping_order_create_page.Input dropdown search    ${site_name}
    shipping_order_create_page.Select item from list by index   ${checkbox_index['first_index']}

Revise destination site
    [Documentation]    ...
    [Arguments]    ${site_name}
    shipping_order_list_page.Click revise destination site
    shipping_order_create_page.Input dropdown search    ${site_name}
    shipping_order_create_page.Select item from list by index   ${checkbox_index['first_index']}

Input target pickup date
    [Documentation]    ...
    [Arguments]    ${date}
    shipping_order_create_page.Click target pickup time
    shipping_order_create_page.Input target pickup time    ${date}
    shipping_order_create_page.Click ok target time

Input target deliver date
    [Documentation]    ...
    [Arguments]    ${date}
    shipping_order_create_page.Click target deliver time
    shipping_order_create_page.Input target deliver time    ${date}
    shipping_order_create_page.Click ok target time

Search by customer name
    [Documentation]    ...
    [Arguments]    ${value}
    BuiltIn.Run keyword and ignore error    shipping_order_list_page.Click more filter shipping order
    shipping_order_list_page.Click customer name
    shipping_order_list_page.Input value on filter in shipping order page  ${value}
    shipping_order_create_page.Select item from list by index   ${checkbox_index['first_index']}
    shipping_order_list_page.Click button inquiry shipping order

Verify that value field of table is correct
    [Arguments]    ${index}    ${value}
    ${locator}    String.Replace string    ${lbl_txt_from_column}    ***index***    ${index}
    SeleniumLibrary.Element text should be    ${locator}    ${value}

Find needed column in shipping order page
    [Arguments]    ${index}    ${number_of_columns}    ${column_name}    ${value}
    ${index}    BuiltIn.Convert to string    ${index}
    ${locator}    String.Replace string    ${lbl_column_name}    ***index***    ${index}
    ${found_column}    BuiltIn.Run keyword and return status    SeleniumLibrary.Element text should be    ${locator}    ${column_name}
    IF    ${found_column}
        shipping_order_feature.Verify that value field of table is correct    ${index}    ${value}
    END
    BuiltIn.Run keyword if    ${index}==${number_of_columns}    BuiltIn.Should be true    ${found_column}
    [Return]    ${found_column}

Get specific number columns of table shipping order page
    [Arguments]    ${column_name}    ${value}
    ${number_of_columns}    shipping_order_list_page.Get number columns of table
    FOR    ${index}    IN RANGE    1    ${number_of_columns}+1
        ${found_column}    shipping_order_feature.Find needed column in shipping order page    ${index}    ${number_of_columns}    ${column_name}    ${value}
        BuiltIn.Exit for loop if    ${found_column}
    END

Search by shipping order status
    [Documentation]    ...
    [Arguments]    ${value}
    shipping_order_list_page.Click more filter shipping order
    shipping_order_list_page.Click select shipping order status     ${value}
    shipping_order_list_page.Click button inquiry shipping order

Revise target pickup date
    [Documentation]    ...
    [Arguments]    ${date}
    shipping_order_list_page.Click revise target pickup time
    shipping_order_list_page.Input revise target pickup time        ${date}
    shipping_order_list_page.Click ok revise target pickup time

Revise target deliver date
    [Documentation]    ...
    [Arguments]    ${date}
    shipping_order_list_page.Click revise target deliver time
    shipping_order_list_page.Input revise target deliver time       ${date}
    shipping_order_list_page.Click ok revise target deliver time