*** Keywords ***
Open TMS website
    [Documentation]    For open TMS website
    [Arguments]            ${browser}=chrome
    ${chrome_options}=     BuiltIn.Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    BuiltIn.Call Method    ${chrome_options}    add_argument    --no-sandbox
    BuiltIn.Call Method    ${chrome_options}    add_argument    --headless
    BuiltIn.Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    BuiltIn.Call Method    ${chrome_options}    add_argument    --allow-insecure-localhost
    SeleniumLibrary.Create WebDriver            Chrome    chrome_options=${chrome_options}
    SeleniumLibrary.Set window size             ${1400}    ${800}
    SeleniumLibrary.Go To                       ${SERVER_CONFIG.SUPPLIER_URL}
    SeleniumLibrary.Set Selenium Speed          ${shortSeleniumSpeed}
    SeleniumLibrary.Set Selenium Timeout        ${GLOBAL_CONFIG.TIME_OUT}

Close TMS website
    BuiltIn.Run keyword and ignore error    SeleniumLibrary.Capture page screenshot
    BuiltIn.Run keyword if test failed      Run keyword and ignore error    SeleniumLibrary.Log source
    SeleniumLibrary.Close all browsers

Wait and click element
    [Arguments]    ${locator}
    SeleniumLibrary.Wait until element is visible    ${locator}    ${GLOBAL_CONFIG.LOW_TIME_OUT}
    SeleniumLibrary.Click element    ${locator}

Wait and double click element
    [Arguments]    ${locator}
    SeleniumLibrary.Wait until element is visible    ${locator}    ${GLOBAL_CONFIG.LOW_TIME_OUT}
    SeleniumLibrary.Double click element    ${locator}

Wait and input text
    [Arguments]    ${locator}   ${value}
    SeleniumLibrary.Wait until element is visible    ${locator}    ${GLOBAL_CONFIG.LOW_TIME_OUT}
    # SeleniumLibrary.Input Text    ${locator}    ${value}
    SeleniumLibrary.Press Keys      ${locator}    ${value}

Select main menu
    [Arguments]    ${menu_name}
    ${menu_locator}    BuiltIn.Evaluate    "${common.li_main_menu}".format("${menu_name}")
    common.Wait and click element    ${menu_locator}

Select sub menu
    [Arguments]    ${menu_name}
    ${menu_locator}    BuiltIn.Evaluate    "${common.li_sub_menu}".format("${menu_name}")
    common.Wait and click element    ${menu_locator}

Verify success message
    [Arguments]    ${msg}
    ${msg_locator}    BuiltIn.Evaluate    "${common.lbl_success_message}".format("${msg}")
    SeleniumLibrary.Wait until element is visible    ${msg_locator}    ${GLOBAL_CONFIG.TIME_OUT}

Verify dialog popup
    [Arguments]    ${msg}
    SeleniumLibrary.Wait until element is visible    ${common.dialog_popup}    ${GLOBAL_CONFIG.TIME_OUT}
    SeleniumLibrary.Element text should be    ${common.dialog_popup}    ${msg}

Click button confirm dialog
    common.Wait and click element    ${common.btn_dialog_confirm}

Click element when ready
    [Documentation]     Keyword to wait for element to be visible before clicking.
    ...     \n default retry clicking is 3 times
    ...     \n can also wait for only page is CONTAINS element instead of visible
    [Arguments]     ${locator}  ${retry}=4      ${only_contains}=${FALSE}       ${timeout}=${GLOBAL_CONFIG.TIME_OUT}
    FOR     ${i}    IN RANGE    1   ${retry}
        IF  ${only_contains}
            ${wait_status}=             Run keyword and ignore error   SeleniumLibrary.Wait until page contains element     ${locator}    ${timeout}
            ${err_msg_wait}=            Convert to string       ${wait_status[1]}
            ${is_not_stale_wait}=       Run keyword and return status    Should not contain     ${err_msg_wait}      StaleElementReferenceException
        ELSE
            SeleniumLibrary.Wait until element is enabled    ${locator}     ${timeout}
            ${wait_status}=             Run keyword and ignore error   SeleniumLibrary.Wait until element is visible        ${locator}   ${timeout}
            ${err_msg_wait}=            Convert to string       ${wait_status[1]}
            ${is_not_stale_wait}=       Run keyword and return status    Should not contain     ${err_msg_wait}      StaleElementReferenceException
        END
        ${is_success}=          Run keyword and ignore error   SeleniumLibrary.Click element   ${locator}
        ${err_msg}=             Convert To String       ${is_success[1]}
        ${is_obsecure}=         Run keyword and return status    Should not contain     ${err_msg}       Other element would receive the click
        ${is_not_stale}=        Run keyword and return status    Should not contain     ${err_msg}       StaleElementReferenceException
        ${is_no_err}=           Run keyword and return status    Should be true        '${err_msg}' == '${NONE}'
        ${is_empty_wait}=       Run keyword and return status    Should be true         '${err_msg_wait}' == '${NONE}'
        ${result}=              BuiltIn.Evaluate    ${is_success} and ${is_not_stale_wait} and ${is_obsecure} and ${is_not_stale} and ${is_no_err} and ${is_empty_wait}
        Exit for loop if        ${result}
        Log     'retry clicking element for ${i} time with error: ${err_msg}, ${err_msg_wait}'   level=WARN
    END
    BuiltIn.Should be true  ${result}   msg="Failed to click element after ${retry} retry"

Input text to element when ready
    [Documentation]     Wait for element to be visible first before input text. Retry 4 times
    [Arguments]     ${locator}     ${text}     ${clear}=${TRUE}     ${timeout}=${GLOBAL_CONFIG.TIME_OUT}
    SeleniumLibrary.Wait until element is visible    ${locator}     ${timeout}
    SeleniumLibrary.Wait until element is enabled    ${locator}     ${timeout}
    FOR    ${index}    IN RANGE    1    4
        ${result_msg}=      Run keyword and ignore error    SeleniumLibrary.Input Text      ${locator}     ${text}     clear=${clear}
        ${err_msg}=         Convert To String       ${result_msg[1]}
        ${is_success}=                  Run keyword and return status    BuiltIn.Should Be Equal        ${err_msg}      None
        ${is_not_loading_error}=        Run keyword and return status    Should Not Contain     ${err_msg}      invalid element state
        Exit For Loop If        ${is_success} or ${is_not_loading_error}
    END
    BuiltIn.Should Be True      ${is_success}   msg=Unable to input text to element after 4 retry

Get text from element when ready
    [Documentation]     Wait until element is visible first before get text from element.
                        ...     \n default timeout is same as ${GLOBAL_TIMEOUT}
    [Arguments]     ${locator}      ${timeout}=${GLOBAL_CONFIG.TIME_OUT}
    SeleniumLibrary.Wait until element is visible    ${locator}     ${timeout}
    ${text}=    SeleniumLibrary.Get text    ${locator}
    [Return]    ${text}

Click confirm button with capital letter
    common.Click element when ready     ${common.btn_confirm_capital}

Click confirm button
    common.Click element when ready     ${common.btn_confirm}