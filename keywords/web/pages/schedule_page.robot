*** Keywords ***
Input dispatch number into textbox
    [Arguments]     ${dispatch_number}
    common.Wait and click element               ${schedule.lbl_dispatch_number}
    common.Input text to element when ready     ${schedule.lbl_dispatch_number}     ${dispatch_number}

Click to select highlight dispatch number result
    [Documentation]     Click to select dispatch number, when input dispatch number will be shown up highlight text number.
    [Arguments]     ${waybill_number}
    ${locator}=     String.Replace string       ${schedule.txt_dispatch_searching}  ***string***   ${waybill_number}
    common.Wait and click element       ${locator}

Click inquire button
    common.Click element when ready     ${delivery_plan.btn_inquire}

Double click checkbox select dispatch number by index
    [Arguments]    ${index}
    ${checkbox_locator}    BuiltIn.Evaluate    "${chk_select_shipping_order}".format("${index}")
    common.Wait and double click element        ${checkbox_locator}

Click add new order after select dispatch number
    common.Wait and click element       ${schedule.btn_add_new_order}

Fill into waybill number textbox
    [Arguments]     ${waybill_number}
    common.Wait and click element               ${schedule.lbl_waybill_number}
    common.Input text to element when ready     ${schedule.lbl_waybill_number}     ${waybill_number}

Select add new order searching result
    [Arguments]     ${waybill_number}
    ${locator}      String.Replace string       ${schedule.lbl_add_new_order}   ***string***    ${waybill_number}
    common.Wait and click element       ${locator}