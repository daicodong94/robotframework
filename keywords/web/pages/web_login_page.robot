*** Keywords ***
Input email TMS website
    [Documentation]    Input email field on Login page TMS website
    [Arguments]    ${email}
    common.Input text to element when ready    ${txt_login_email}    ${email}

Input password TMS website
    [Documentation]    Input password field on Login page TMS website
    [Arguments]    ${password}
    common.Input text to element when ready    ${txt_login_password}    ${password}

Click login button
    [Documentation]    Click login button on Login page TMS website
    common.Click element when ready            ${btn_login}
    ${menu_locator}    BuiltIn.Evaluate    "${common.li_main_menu}".format("${menu['lbl_shipping_order']}")
    SeleniumLibrary.Wait until element is visible               ${menu_locator}

Click remember me checkbox
    [Documentation]    Click remember me to remember who login in website
    common.Wait and click element    ${btn_remember_me}