*** Keywords ***
Search by order number
    [Arguments]      ${order_no}
    common.Wait and input text         ${abnormal.txt_order_no}         ${order_no}
    abnormal_page.Click inquire button

Click inquire button
    common.Click element when ready     ${abnormal.btn_inquire}

Get number columns of table
    SeleniumLibrary.Wait until element is visible    ${abnormal.lbl_number_column}    ${GLOBAL_CONFIG.TIME_OUT}
    ${count}    SeleniumLibrary.Get element count    ${abnormal.lbl_number_column}
    [Return]    ${count}

Check abnormal status of order with refer to row
    [Arguments]      ${abnormal_status}      ${row}
    ${locator}    String.Replace string    ${abnormal.lbl_column_abnormal_status}    ***index***    ${row}
    SeleniumLibrary.Element text should be   ${locator}    ${abnormal_status}


Check source status of order with refer to row
    [Arguments]      ${source_status}      ${row}
    ${locator}    String.Replace string    ${abnormal.lbl_column_source_status}    ***index***    ${row}
    SeleniumLibrary.Element text should be   ${locator}    ${source_status}