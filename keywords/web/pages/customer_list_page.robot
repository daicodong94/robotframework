*** Keywords ***
Input search keyword on search box
    [Documentation]    Input some keyword into search box
    [Arguments]    ${value}
    common.Input text to element when ready    ${txt_search}    ${value}

Click search button
    [Documentation]    Click search button on Customer tab
    common.Click element when ready    ${btn_search}

Check search result
    [Documentation]    Check search result be same as input_customer_search
    SeleniumLibrary.Wait until element is visible      ${customer_search_result}
    ${txt_customer_search_result}=       SeleniumLibrary.Get text       ${customer_search_result}
    BuiltIn.Should be equal     ${txt_customer_search_result}        ${TC_master_data_customer['customer_id']}

Click filter button
    [Documentation]    Click filter button on Customer tab
    common.Click element when ready            ${btn_customer_filter}

Click dropdown customer type
    [Documentation]    Click dropdown customer type on Customer tab
    common.Click element when ready            ${ddl_customer_type}

Click select customer type in dropdown list
    [Documentation]    Click select customer type in dropdown list on Customer tab
    [Arguments]     ${value}
    ${customer_type_locator}=     String.Replace string       ${ddl_customer_type_select}  ***string***  ${value}
    common.Click element when ready       ${customer_type_locator}

Check reset filter button is appeared
    [Documentation]    Check reset filter button is appeared on Customer tab
    SeleniumLibrary.Wait until element is visible   ${btn_customer_reset_filter}

Check filter result
    [Documentation]    Check filter result on Customer tab
    SeleniumLibrary.Wait until element is visible      ${customer_filter_result}
    ${txt_filter_result}=       SeleniumLibrary.Get text       ${customer_filter_result}
    BuiltIn.Should be equal     ${txt_filter_result}        ${master_data['customer_personal']}

Click edit button
    [Documentation]    Click edit button on Cutomer tab
    common.Click element when ready          ${btn_customer_edit}
