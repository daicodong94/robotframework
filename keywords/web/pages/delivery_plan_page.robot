*** Keywords ***
Select a desire waybill number list
    [Arguments]     ${waybill_number}
    ${locator}=     String.Replace string       ${delivery_plan.selected_order}  ***string***    ${waybill_number}
    common.Click element when ready     ${locator}

Click withdraw button
    common.Click element when ready     ${delivery_plan.btn_withdraw}

Click inquire button
    common.Click element when ready     ${delivery_plan.btn_inquire}

Click create a dispatch order button
    common.Click element when ready     ${delivery_plan.btn_create_dispatch}

Click cancel button when dialog popup displayed
    common.Click element when ready     ${delivery_plan.btn_cancel}

Click delete button
    common.Click element when ready     ${delivery_plan.btn_delete}

Click add to button
    common.Click element when ready     ${delivery_plan.btn_add_to}

Get waybill number by using order number
    [Arguments]     ${order_number}
    ${order_number}=        BuiltIn.Evaluate   ",".join(${order_number})
    ${waybill_number}=      String.Remove String    ${order_number}    O
    [Return]    ${waybill_number}

Verify confirm popup displayed when clicked withdraw
    SeleniumLibrary.Wait until element is visible       ${delivery_plan.txt_confirm_operation}

Input waybill number into textbox
    [Arguments]     ${waybill_number}
    common.Wait and click element               ${delivery_plan.lbl_waybill_number}
    common.Input text to element when ready     ${delivery_plan.lbl_waybill_number}      ${waybill_number}

Input dispatch number into textbox
    [Arguments]     ${dispatch_number}
    common.Wait and click element               ${delivery_plan.lbl_dispatch_number}
    common.Input text to element when ready     ${delivery_plan.lbl_dispatch_number}     ${dispatch_number}

Verify that inquire result correctly displayed
    [Arguments]     ${waybill_number}
    ${locator}=     String.Replace string       ${delivery_plan.selected_order}  ***string***   ${waybill_number}
    SeleniumLibrary.Wait until element is visible       ${locator}

Check if dialog popup displayed when create a dispatch order
    ${check_visible}=     BuiltIn.Run keyword and return status     SeleniumLibrary.Wait until element is visible     ${delivery_plan.lbl_create_dispatch_popup}
    IF      '${check_visible}'=='True'
        delivery_plan_page.Click cancel button when dialog popup displayed
    END

Click to select highlight dispatch number result
    [Documentation]     Click to select dispatch number, when input dispatch number will be shown up highlight text number.
    [Arguments]     ${waybill_number}
    ${locator}=     String.Replace string       ${delivery_plan.txt_dispatch_searching}  ***string***   ${waybill_number}
    common.Wait and click element       ${locator}

Get dispatch number from searching result
    ${dispatch_number}=       common.Get text from element when ready     ${delivery_plan.txt_dispatch_result}
    [Return]    ${dispatch_number}

Click checkbox to select dispatch number
    [Arguments]     ${dispatch_number}
    ${locator}=     String.Replace string       ${delivery_plan.chk_select_dispatch_num}  ***string***   ${dispatch_number}
    common.Wait and click element       ${locator}

Verify confirm delete message displayed
    SeleniumLibrary.Wait until element is visible       ${delivery_plan.lbl_confirm_delete}
