*** Keywords ***
Verify home page is visible
    SeleniumLibrary.Wait until element is visible    ${home.lbl_home_page}

Verify default language is english
    ${get_home_lang}=       common.Get text from element when ready     ${home.lbl_home_page}
    ${default_home_lang}=   BuiltIn.Run keyword and return status   BuiltIn.Should be equal     '${get_home_lang}'      'Home'
    [Return]    ${default_home_lang}