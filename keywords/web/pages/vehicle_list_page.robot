*** Keywords ***
Input search keyword on search box
    [Documentation]    Input some keyword into search box
    [Arguments]    ${value}
    common.Input text to element when ready    ${txt_vehicle_search}    ${value}

Click search button
    [Documentation]    Click search button on vehicle tab
    common.Click element when ready    ${btn_vehicle_search}

Check search result
    [Documentation]    Check search result be same as txt_vehicle_search
    SeleniumLibrary.Wait until element is visible      ${vehicle_search_result}
    ${txt_vehicle_search_result}=       SeleniumLibrary.Get text       ${vehicle_search_result}
    BuiltIn.Should be equal     ${txt_vehicle_search_result}        ${TC_master_data_vehicle['vehicle_id']}