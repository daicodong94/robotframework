*** Keywords ***
Select item from list by index
    [Documentation]    ...
    [Arguments]    ${index}
    ${item_locator}    BuiltIn.Evaluate     "${li_candidate_item}".format("${index}")
    common.Wait And Click Element    ${item_locator}

Input dropdown search
    [Documentation]    ...
    [Arguments]    ${value}
    common.Wait and input text    ${txt_search_keyword}    ${value}

Click target pickup time
    [Documentation]    ...
    common.Wait And Click Element    ${ddl_target_pickup_time}

Click target deliver time
    [Documentation]    ...
    common.Wait And Click Element    ${ddl_target_deliver_time}

Input target pickup time
    [Documentation]    ...
    [Arguments]    ${value}
    common.Wait and input text    ${txt_pickup_delivery_date}    ${value}

Input target deliver time
    [Documentation]    ...
    [Arguments]    ${value}
    common.Wait and input text    ${txt_pickup_delivery_date}    ${value}

Click ok target time
    [Documentation]    ...
    common.Wait And Click Element    ${btn_date_ok}
