*** Keywords ***
Wait and click element
    [Arguments]    ${locator}
    SeleniumLibrary.Wait until element is visible    ${locator}    ${GLOBAL_CONFIG.LOW_TIME_OUT}
    SeleniumLibrary.Click element    ${locator}

Click to select new button
    [Documentation]    ...
    master_data_service_product_page.Wait and click element    ${btn_new}

Input service product name
    [Documentation]    ...
    [Arguments]     ${value}
    common.Wait and click element    ${txt_service_product_name}
    common.Wait and input text    ${txt_service_product_name}    ${value}

Input service product code
    [Documentation]    ...
    [Arguments]     ${value}
    common.Wait and click element    ${txt_service_product_code}
    common.Wait and input text    ${txt_service_product_code}    ${value}

Click to select project attribute
    [Documentation]    ...
    common.Wait and click element    ${ddl_click_attributes_list}
    common.Wait and click element    ${ddl_select_service_product_attributes}

Input service product short name
    [Documentation]    ...
    [Arguments]     ${value}
    common.Wait and click element    ${txt_service_short_name}
    common.Wait and input text    ${txt_service_short_name}    ${value}

Input service product ratio
    [Documentation]    ...
    [Arguments]     ${value}
    common.Wait and click element    ${txt_service_ratio}
    common.Wait and input text    ${txt_service_ratio}    ${value}

Click save to create a new service product
    [Documentation]    ...
    common.Wait and click element    ${btn_save}

Click delete a service product
    [Documentation]    ...
    common.Wait and click element    ${btn_delete}

Click button confirm dialog
    [Documentation]    ...
    common.Wait and click element    ${btn_confirm}
