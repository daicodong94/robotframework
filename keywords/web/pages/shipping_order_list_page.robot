*** Keywords ***
Select shipping order action
    [Documentation]    ...
    [Arguments]    ${action}
    ${btn_action_locator}    String.Replace string    ${btn_shipping_order_action}    ***btn***    ${action}
    common.Wait and click element       ${btn_action_locator}

Click more filter shipping order
    [Documentation]    ...
    common.Click element when ready     ${lnk_more_filter}

Click collapse filter shipping order
    common.Click element when ready    ${btn_collapse_filter}

Input filter by shipping order number
    [Documentation]    ...
    [Arguments]    ${shipping_order_no}
    common.Wait and click element               ${txt_shipping_order_no}
    common.Input text to element when ready     ${txt_shipping_order_no}    ${shipping_order_no}

Input filter by new shipping order number
    [Arguments]    ${old_shipping_order_no}    ${new_shipping_order_no}
    common.Wait and click element               ${txt_shipping_order_no}
    shipping_order_list_page.Mouse over and delete last data in shipping order number field    ${old_shipping_order_no}
    common.Wait and click element               ${txt_shipping_order_no}
    common.Input text to element when ready     ${txt_shipping_order_no}    ${new_shipping_order_no}

Mouse over and delete last data in shipping order number field
    [Arguments]    ${shipping_order_no}
    ${shipping_order_no_locator}    String.Replace string     ${txt_old_shipping_order_number}    ***old_shipping_order_no***    ${shipping_order_no}
    SeleniumLibrary.Mouse over    ${shipping_order_no_locator}
    common.Click element when ready    ${btn_delete_shipping_order_number}

Click customer SAP code
    common.Wait and click element               ${ddl_customer_sap_code}

Select filter by customer SAP code
    [Arguments]    ${customer_sap_code}
    ${customer_sap_code_locator}    String.Replace string    ${lbl_customer_sap_code}    ***sap_code***    ${customer_sap_code}
    common.Wait and click element    ${customer_sap_code_locator}

Click button inquiry shipping order
    [Documentation]    ...
    common.Wait and click element    ${btn_inquire}

Click checkbox select shipping order by index
    [Documentation]    ...
    [Arguments]    ${index}
    ${checkbox_locator}    BuiltIn.Evaluate    "${chk_select_shipping_order}".format("${index}")
    common.Wait and click element    ${checkbox_locator}

Click revise customer
    [Documentation]    ...
    common.Wait and click element    ${ddl_revise_customer_name}

Click revise origin site
    [Documentation]    ...
    common.Wait and click element    ${ddl_revise_origin_site}

Click revise destination site
    [Documentation]    ...
    common.Wait and click element    ${ddl_revise_destination_site}

Click confirm revise
    [Documentation]    ...
    common.Wait and click element    ${btn_confirm_revise}

Input value on filter in shipping order page
    [Arguments]    ${value}
    common.Wait and input text    ${txt_filter}    ${value}

Verify list of search result with customer sap code
    [Documentation]    ...
    [Arguments]    ${value}
    ${search_result}    SeleniumLibrary.Get WebElements    ${tr_shipping_order_row}
    ${count}    BuiltIn.Get length    ${search_result}
    FOR    ${index}    IN RANGE    1    ${count}
        ${locator}    BuiltIn.Evaluate    "${lbl_td_customer_sap_code}".format("${index}")
        SeleniumLibrary.Element should contain    ${locator}    ${value}
    END

Verify shipping order status
    [Documentation]    ....
    [Arguments]    ${order_no}    ${status}
    ${order_no_locator}    BuiltIn.Evaluate    "${lbl_td_order_no}".format("1")
    ${order_status_locator}    BuiltIn.Evaluate    "${lbl_td_order_status}".format("1")
    SeleniumLibrary.Element text should be    ${order_no_locator}    ${order_no}
    SeleniumLibrary.Element text should be    ${order_status_locator}    ${status}

Click select shipping order status
    [Documentation]    ...
    [Arguments]    ${status}
    common.Wait and click element    ${txt_filter_status}
    ${status_locator}    BuiltIn.Evaluate    "${lbl_li_status}".format("${status}")
    common.Wait and click element    ${status_locator}

Double click on shipping order by index
    [Documentation]    ....
    [Arguments]    ${index}
    ${order_no_locator}    BuiltIn.Evaluate    "${lbl_td_order_no}".format("${index}")
    SeleniumLibrary.Wait until element is visible    ${order_no_locator}    ${GLOBAL_CONFIG.LOW_TIME_OUT}
    SeleniumLibrary.Double click element    ${order_no_locator}

Click revise target pickup time
    common.Wait and click element    ${txt_revise_target_pickup_date}

Click revise target deliver time
    common.Wait and click element    ${txt_revise_target_deliver_date}

Input revise target pickup time
    [Arguments]    ${value}
    common.Wait and input text    ${txt_revise_pickup_date}    ${value}

Input revise target deliver time
    [Arguments]    ${value}
    common.Wait and input text    ${txt_pickup_delivery_date}    ${value}

Click ok revise target pickup time
    common.Wait and click element    ${btn_pick_date_ok}

Click ok revise target deliver time
    common.Wait and click element    ${btn_deliver_date_ok}

Click customer name
    common.Wait and click element    ${txt_customer_name}

Get number columns of table
    ${count}    SeleniumLibrary.Get element count    ${lbl_number_column}
    [Return]    ${count}

Verify scheduling page
    SeleniumLibrary.Wait until element is visible    ${lbl_scheduling_page}    ${GLOBAL_CONFIG.LOW_TIME_OUT}