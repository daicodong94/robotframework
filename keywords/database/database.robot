*** Keywords ***
Get message id from DB by job no
    database.Connect database
    ${message_id}   databaselibrary.Query   select message_id from message_dev.message_notification_process where ref_id = '${job_no}'
    RETURN    ${message_id}[0][0]

Connect database
    databaselibrary.Connect to database    pymysql    ${DATABASE.DATABASE_NAME}    ${DATABASE.USERNAME}    ${DATABASE.PASSWORD}    ${DATABASE.SERVER_HOST}    ${DATABASE.PORT}
