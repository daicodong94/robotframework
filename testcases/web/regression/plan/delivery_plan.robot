*** Settings ***
Resource        ${CURDIR}/../../../../resources/imports.robot
Test Setup      common.Open TMS website
Test Teardown   common.Close TMS website

*** Test Cases ***
Verify function fixed line matches with the system design
    [Tags]      TC_delivery_plan_04     web     web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    common.Select main menu     ${menu['lbl_plan']}
    common.Select sub menu      ${menu['lbl_delivery_plan']}
    delivery_plan_feature.Select waybill list by using order number     ${shipping_order_no}
    delivery_plan_page.Click withdraw button
    common.Verify dialog popup          ${dialog_popup_msg['lbl_confirm_operation']}
    common.Click confirm button
    common.Verify success message       ${dialog_popup_msg['lbl_done']}

Verify function inquire matches with the system design
    [Tags]      TC_delivery_plan_05     web     web_ready    web_high
    login_feature.Login to TMS website and get cookie       ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    common.Select main menu     ${menu['lbl_plan']}
    common.Select sub menu      ${menu['lbl_delivery_plan']}
    ${waybill_number}       delivery_plan_page.Get waybill number by using order number    ${shipping_order_no}
    delivery_plan_page.Input waybill number into textbox        ${waybill_number}
    delivery_plan_page.Click inquire button
    delivery_plan_feature.Select waybill list by using order number         ${shipping_order_no}
    delivery_plan_page.Verify that inquire result correctly displayed       ${waybill_number}

Verify function delete matches with the system design
    [Tags]      TC_delivery_plan_06     web     web_ready    web_high
    login_feature.Login to TMS website and get cookie       ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    common.Select main menu     ${menu['lbl_plan']}
    common.Select sub menu      ${menu['lbl_delivery_plan']}
    ${waybill_number}       delivery_plan_page.Get waybill number by using order number    ${shipping_order_no}
    delivery_plan_feature.Select waybill list by using order number         ${shipping_order_no}
    delivery_plan_feature.Click create dispatch order and check dialog popup
    common.Verify success message       ${dialog_popup_msg['lbl_create_success']}
    ${dispatch_number}=     delivery_plan_feature.Searching dispatch number by using waybill number     ${waybill_number}
    delivery_plan_page.Click checkbox to select dispatch number     ${dispatch_number}
    delivery_plan_page.Click delete button
    delivery_plan_page.Verify confirm delete message displayed
    common.Click confirm button
    common.Verify success message       ${dialog_popup_msg['lbl_successfully_deleted']}

Verify function confirm matches with the system design
    [Tags]      TC_delivery_plan_07     web     web_ready    web_high
    login_feature.Login to TMS website and get cookie       ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    common.Select main menu     ${menu['lbl_plan']}
    common.Select sub menu      ${menu['lbl_delivery_plan']}
    ${waybill_number}       delivery_plan_page.Get waybill number by using order number    ${shipping_order_no}
    delivery_plan_feature.Select waybill list by using order number         ${shipping_order_no}
    delivery_plan_feature.Click create dispatch order and check dialog popup
    common.Verify success message       ${dialog_popup_msg['lbl_create_success']}
    ${dispatch_number}=     delivery_plan_feature.Searching dispatch number by using waybill number     ${waybill_number}
    delivery_plan_page.Click checkbox to select dispatch number     ${dispatch_number}
    common.Click confirm button with capital letter
    common.Verify success message       ${dialog_popup_msg['lbl_done']}

Verify function add to matches with the system design
    [Tags]      TC_delivery_plan_08     web     web_ready   web_high
    login_feature.Login to TMS website and get cookie       ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    ${shipping_order_no_2}  order_api_feature.Created shipping order       ${shipping_orders.TC01}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    common.Select main menu     ${menu['lbl_plan']}
    common.Select sub menu      ${menu['lbl_delivery_plan']}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no_2[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    common.Select sub menu      ${menu['lbl_delivery_plan']}
    ${waybill_number}       delivery_plan_page.Get waybill number by using order number    ${shipping_order_no}
    delivery_plan_feature.Select waybill list by using order number         ${shipping_order_no}
    delivery_plan_feature.Click create dispatch order and check dialog popup
    common.Verify success message       ${dialog_popup_msg['lbl_create_success']}
    ${dispatch_number}=     delivery_plan_feature.Searching dispatch number by using waybill number     ${waybill_number}
    delivery_plan_page.Click checkbox to select dispatch number     ${dispatch_number}
    ${waybill_number_2}       delivery_plan_page.Get waybill number by using order number    ${shipping_order_no_2}
    delivery_plan_feature.Select waybill list by using order number         ${shipping_order_no_2}
    delivery_plan_page.Click add to button
    common.Verify success message       ${dialog_popup_msg['lbl_create_success']}