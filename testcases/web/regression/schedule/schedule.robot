*** Settings ***
Resource        ${CURDIR}/../../../../resources/imports.robot
Test Setup        common.Open TMS website
Test Teardown     common.Close TMS website

*** Test Cases***
Verify that add new order function on schedule page works correctly
    [Tags]      TC_schedule_02      web     web_ready   web_high
    login_feature.Login to TMS website and get cookie       ${users.tms_user}    ${users.tms_web_password}
    ${order_detail}           order_api_feature.Create shipping order and assign the job to driver      ${shipping_orders.TC03}
    ${shipping_order_no_2}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    common.Select main menu     ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number         ${shipping_order_no_2[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_confirm']}
    ${waybill_number_2}       delivery_plan_page.Get waybill number by using order number    ${shipping_order_no_2}
    common.Select main menu     ${menu['lbl_schedule']}
    schedule_feature.Input dispatch number with order number then search    ${order_detail[0].order_no}
    schedule_page.Double click checkbox select dispatch number by index     ${checkbox_index['first_index']}
    schedule_feature.Add new order for accepted jobs    ${waybill_number_2}
    common.Click confirm button with capital letter
    common.Verify success message       ${dialog_popup_msg['lbl_done']}