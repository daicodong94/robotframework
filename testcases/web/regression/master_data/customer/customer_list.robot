*** Settings ***
Resource        ${CURDIR}/../../../../../resources/imports.robot
Test Setup      common.Open TMS website
Test Teardown   common.Close TMS website

*** Test Cases ***
Verify search on list screen
    [Tags]      TC_master_data_check_sub_menu_customer_01    web    web_ready    web_high

    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    common.Select main menu  ${menu['lbl_master_data']}
    common.Select sub menu   ${menu['lbl_sub_menu_customer']}
    customer_list_feature.Search keyword and check result

Verify select data of all fields on filter
    [Tags]      TC_master_data_check_sub_menu_customer_02    web    web_ready    web_high

    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    common.Select main menu  ${menu['lbl_master_data']}
    common.Select sub menu   ${menu['lbl_sub_menu_customer']}
    customer_list_feature.Filter with customer type
    customer_list_feature.Check reset filter button and filter result
