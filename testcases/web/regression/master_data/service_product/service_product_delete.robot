*** Settings ***
Resource          ${CURDIR}/../../../../../resources/imports.robot
Test Setup        common.Open TMS website
Test Teardown     common.Close TMS website

*** Test Cases ***
Verify delete a service product
    [Tags]      TC_md_service_product_02    web    web_ready
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    common.Select main menu         ${menu['lbl_master_data']}
    common.Select sub menu          ${menu['lbl_service_product']}
    master_data_service_product_page.Click to select new button
    master_data_service_product_feature.Add new service product    ${service_product_page.txt_service_product_name}    ${service_product_page.txt_service_product_code}    ${service_product_page.txt_service_short_name}    ${service_product_page.txt_service_ratio}
    master_data_service_product_page.Click delete a service product
    common.Verify dialog popup      ${dialog_popup_msg['lbl_confirm_delete']}
    master_data_service_product_page.Click button confirm dialog
    common.Verify success message    ${dialog_popup_msg['lbl_delete_success']}