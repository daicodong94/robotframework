*** Settings ***
Resource          ${CURDIR}/../../../../../resources/imports.robot
Test Setup        common.Open TMS website
Test Teardown     common.Close TMS website

*** Test Cases ***
Verify add a new service product
    [Tags]      TC_md_service_product_01    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    common.Select main menu         ${menu['lbl_master_data']}
    common.Select sub menu          ${menu['lbl_service_product']}
    master_data_service_product_page.Click to select new button
    master_data_service_product_feature.Add new service product    ${service_product_page.txt_service_product_name}    ${service_product_page.txt_service_product_code}    ${service_product_page.txt_service_short_name}    ${service_product_page.txt_service_ratio}
    common.Verify success message    ${dialog_popup_msg['lbl_successfully_saved']}
    master_data_service_product_page.Click delete a service product
    master_data_service_product_page.Click button confirm dialog