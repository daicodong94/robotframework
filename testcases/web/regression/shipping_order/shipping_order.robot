*** Settings ***
Resource          ${CURDIR}/../../../../resources/imports.robot
Test Setup        common.Open TMS website
Test Teardown     common.Close TMS website

*** Test Cases ***
Verify revise function work and display correctly
    [Tags]      TC_shipping_order_01    web
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}    order_api_feature.Created shipping order       ${shipping_orders.TC01}
    ${today}=               common.Generate Date    ${date.current_date}
    ${tomorrow}=            common.Generate Date    ${date.tomorrow_date}
    common.Select main menu         ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number      ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_revise']}
    shipping_order_feature.Revise Customer                 ${customers.reg_customer_name}
    shipping_order_feature.Revise origin site              ${sites.automation_site_name_1}
    shipping_order_feature.Revise destination site         ${sites.automation_site_name_2}
    shipping_order_feature.Revise target pickup date       ${today}
    shipping_order_feature.Revise target deliver date      ${tomorrow}
    shipping_order_list_page.Click confirm revise
    common.Verify success message   ${dialog_popup_msg['lbl_sucessfully_modified']}

Verify delete function work and display correctly
    [Tags]      TC_shipping_order_02    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}=   order_api_feature.Created shipping order        ${shipping_orders.TC01}
    ${today}=               common.Generate Date    ${date.current_date}
    ${tomorrow}=            common.Generate Date    ${date.tomorrow_date}
    common.Select main menu         ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number      ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index      ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action       ${shipping_order['btn_delete']}
    common.Verify dialog popup      ${dialog_popup_msg['lbl_delete']}
    common.Click button confirm dialog
    common.Verify success message   ${dialog_popup_msg['lbl_done']}

Verify check inquired fuction match with customer SAP code
    [Tags]      TC_shipping_order_03    web    web_ready
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}=   order_api_feature.Created shipping order        ${shipping_orders.TC01}
    ${customer_sap_code}=   shipping_order_api.Get latest customer SAP code from customer name    ${shipping_orders.TC01}
    ${today}=               common.Generate Date    ${date.current_date}
    ${tomorrow}=            common.Generate Date    ${date.tomorrow_date}
    common.Select main menu         ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by customer SAP code      ${customer_sap_code}
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_customer_sap_code']}    ${customer_sap_code}

Verify check function withdraw matches with the system design
    [Tags]      TC_shipping_order_05    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}=   order_api_feature.Created shipping order      ${shipping_orders.TC01}
    common.Select main menu                 ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number   ${shipping_order_no[0]}
    shipping_order_list_page.Click checkbox select shipping order by index     ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action            ${shipping_order['btn_confirm']}
    shipping_order_list_page.Click select shipping order status      ${order_status['order_confirmed']}
    shipping_order_list_page.Click button inquiry shipping order
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_shipping_order_number']}    ${shipping_order_no[0]}
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_status_column']}    ${order_status['order_confirmed']}
    shipping_order_list_page.Click checkbox select shipping order by index   ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action    ${shipping_order['btn_withdraw']}
    common.Verify dialog popup      ${dialog_popup_msg['lbl_confirm_operation']}
    common.Click button confirm dialog
    common.Verify success message   ${dialog_popup_msg['lbl_withdraw_successfully']}
    shipping_order_list_page.Click select shipping order status    ${order_status['order_created']}
    shipping_order_list_page.Click button inquiry shipping order
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_shipping_order_number']}    ${shipping_order_no[0]}
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_status_column']}    ${order_status['order_created']}

Verify that copy a new order - Function Save
    [Tags]      TC_shipping_order_06    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}=   order_api_feature.Created shipping order        ${shipping_orders.TC01}
    ${today}=               common.Generate Date    ${date.current_date}
    ${tomorrow}=            common.Generate Date    ${date.tomorrow_date}
    common.Select main menu         ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number      ${shipping_order_no[0]}
    shipping_order_list_page.Double click on shipping order by index    ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action       ${shipping_order['btn_copy_a_new_order']}
    common.Verify success message          ${dialog_popup_msg['lbl_copy_new_order_success']}
    shipping_order_feature.Input target pickup date        ${today}
    shipping_order_feature.Input target deliver date       ${tomorrow}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_save_capital']}
    common.Verify success message          ${dialog_popup_msg['lbl_successfully_saved']}
    ${order_no}    shipping_order_api.Get latest shipping order no from customer name      ${customer_name}
    shipping_order_list_page.Input filter by new shipping order number    ${shipping_order_no[0]}    ${order_no}
    shipping_order_list_page.Click button inquiry shipping order
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_shipping_order_number']}    ${order_no}
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_status_column']}    ${order_status['order_created']}

Verify check copy a new order - Function Confirm
    [Tags]      TC_shipping_order_07    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}=   order_api_feature.Created shipping order        ${shipping_orders.TC01}
    ${today}=               common.Generate Date    ${date.current_date}
    ${tomorrow}=            common.Generate Date    ${date.tomorrow_date}
    common.Select main menu         ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number      ${shipping_order_no[0]}
    shipping_order_list_page.Double click on shipping order by index    ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action       ${shipping_order['btn_copy_a_new_order']}
    common.Verify success message          ${dialog_popup_msg['lbl_copy_new_order_success']}
    shipping_order_feature.Input target pickup date        ${today}
    shipping_order_feature.Input target deliver date       ${tomorrow}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_capital_confirm']}
    common.Verify success message          ${dialog_popup_msg['lbl_done']}
    common.Select main menu    ${menu['lbl_plan']}
    common.Select sub menu     ${menu['lbl_delivery_plan']}
    shipping_order_feature.Search by customer name     ${customer_name}
    delivery_plan_feature.Get specific number columns of table plan page    ${shipping_order['lbl_customer_name_column']}    ${customer_name}

Verify check updating shipping order - function copy a new order - direct schedule
    [Tags]      TC_shipping_order_08    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}
    ${shipping_order_no}=   order_api_feature.Created shipping order        ${shipping_orders.TC01}
    ${today}=               common.Generate date    ${date.current_date}
    ${tomorrow}=            common.Generate date    ${date.tomorrow_date}
    common.Select main menu         ${menu['lbl_shipping_order']}
    shipping_order_feature.Search by shipping order number      ${shipping_order_no[0]}
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_shipping_order_number']}    ${shipping_order_no[0]}
    shipping_order_feature.Get specific number columns of table shipping order page    ${shipping_order['lbl_status_column']}    ${order_status['order_created']}
    shipping_order_list_page.Double click on shipping order by index    ${checkbox_index['first_index']}
    shipping_order_list_page.Select shipping order action    ${shipping_order['btn_copy_a_new_order']}
    common.Verify success message    ${dialog_popup_msg['lbl_copy_new_order_success']}
    shipping_order_feature.Input target pickup date    ${today}
    shipping_order_feature.Input target deliver date    ${tomorrow}
    shipping_order_list_page.Select shipping order action          ${shipping_order['btn_direct_schedule']}
    shipping_order_list_page.Verify scheduling page