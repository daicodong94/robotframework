*** Settings ***
Resource          ${CURDIR}/../../../../resources/imports.robot
Test Setup        common.Open TMS website
Test Teardown     common.Close TMS website

*** Test Cases ***
Verify user login TMS system successfully
    [Tags]    TC_login_01    web    web_ready    web_high
    login_feature.Login to TMS website and get cookie      ${users.tms_user}    ${users.tms_web_password}

Verify login account is remembered after login successfully
    [Tags]    TC_login_02    web    web_ready    web_high
    login_feature.Login to TMS website with remember me and get cookie    ${users.tms_user}    ${users.tms_web_password}
    SeleniumLibrary.Go to    ${SERVER_CONFIG.SUPPLIER_URL}
    home_page.Verify home page is visible
