*** Settings ***
Documentation  Abnormal page
Resource        ${CURDIR}/../../../../resources/imports.robot
Test Setup        common.Open TMS website
Test Teardown     common.Close TMS website

*** Test Cases ***
Verify abnormal normal order if order is pickup , delivery late and delivery over temperature
    [Tags]       TC_abnormal_01      TC_abnormal_02     web     web_ready    web_high
    login_feature.Login to TMS website and get cookie       ${users.tms_user}    ${users.tms_web_password}
    ${order_detail}    order_api_feature.Create shipping order and assign the job to driver      ${shipping_orders.TC03}
    order_status_api.Send and update shipment to on the way        ${order_detail}
    order_status_api.Send and update shipment to delivered         ${order_detail}
    common.Select main menu                     ${menu['lbl_abnormal']}
    abnormal_page.Search by order number        ${order_detail[0].order_no}
    abnormal_feature.Order should be correct with data           ${abnormals.pickup_late}        ${abnormals.abnormal_status}       ${abnormals.source_status}
    abnormal_feature.Order should be correct with data           ${abnormals.delivery_late}        ${abnormals.abnormal_status}       ${abnormals.source_status}
