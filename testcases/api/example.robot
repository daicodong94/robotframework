*** Settings ***
Resource    ${CURDIR}/../../resources/imports.robot

*** Test Cases ***
Login TMS
    ${login_cp_ticket_dev}    Post Login To TMS Web    ${users.tms_user}    ${users.tms_password}
    BuiltIn.Log    login_cp_ticket_dev=${login_cp_ticket_dev}
    Get Sites List    ${login_cp_ticket_dev}

Random String
    ${result1}    common.Generate random number    13
    ${result2}    common.Generate random text    5    LOWER
    ${result3}    common.Generate random text    5    UPPER
    ${result4}    common.Generate random text    5    LETTERS
    BuiltIn.Log    ${result1} | ${result2} | ${result3} | ${result4}
    ${start_date}    common.Generate status date    0
    BuiltIn.Log    ${start_date}

Create Customer-Site
    ${login_cp_ticket_dev}    Post Login To TMS Web    ${users.tms_user}    ${users.tms_password}
    ${tax_id}        common.Generate random number    13
    ${c_regis_name}     BuiltIn.Set variable    REG${tax_id}
    ${c_name_th}        BuiltIn.Set variable    บริษัท ${c_regis_name} จำกัด
    ${c_name_en}        BuiltIn.Set variable    Company ${c_regis_name} Ltd
    ${c_sap_code}       BuiltIn.Set variable    REG${tax_id}
    ${c_status_start_date}    common.Generate status date    0
    ${c_phone}       common.Generate random number    8
    ${customer_items}   customer_api.Post create customer    ${login_cp_ticket_dev}
    ...    ${tax_id}    ${c_regis_name}    ${c_name_th}    ${c_name_en}
    ...    ${c_sap_code}    ${c_status_start_date}    ${c_phone}
    # Get Customer Detail    ${login_cp_ticket_dev}    ${customer_items}
    ${site_items}       Post Create Site    ${login_cp_ticket_dev}
    ...    ${customer_items}    ${c_regis_name}    Site-${c_regis_name}
    ...    ${c_status_start_date}    ${c_phone}

Create Driver
    ${login_cp_ticket_dev}    Post Login To TMS Web    ${users.tms_user}    ${users.tms_password}
    ${random_string}    common.Generate random text    5    LETTERS
    ${dr_phone}         common.Generate random number    8
    ${dr_id}            common.Generate random number    13
    ${dr_license_id}    common.Generate random number    16
    ${dr_status_start_date}    common.Generate status date    0
    driver_api.Post create driver    ${login_cp_ticket_dev}    REG${random_string}    0${dr_phone}    ${dr_id}    ${dr_license_id}    ${dr_status_start_date}    8948

Create Sub Contractor
    ${login_cp_ticket_dev}    Post Login To TMS Web    ${users.tms_user}    ${users.tms_password}
    ${tax_id}       common.Generate random number    13
    ${s_regis_name}    BuiltIn.Set variable    REG${tax_id}
    ${s_name_th}    BuiltIn.Set variable    บริษัท ${s_regis_name} จำกัด
    ${s_name_en}    BuiltIn.Set variable    Company ${s_regis_name} Ltd
    ${s_sap_code}   BuiltIn.Set variable    REG${tax_id}
    ${s_status_start_date}    common.Generate status date    0
    ${s_phone}      common.Generate random number    8
    ${s_bank_no}    common.Generate random number    10
    ${sub_contractor_id}    subcontractor_api.Post create sub contractor    ${login_cp_ticket_dev}    ${tax_id}    ${s_regis_name}    ${s_name_th}    ${s_name_en}    ${s_phone}    ${s_sap_code}    ${s_status_start_date}    ${s_bank_no}    ${s_name_th}
    subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}    ${sub_contractor_id}

Create Master Data Flow
    ${login_cp_ticket_dev}      Post Login To TMS Web    ${users.tms_user}    ${users.tms_password}
    ${status_start_date}        common.Generate status date    0
    ${c_tax_id}         common.Generate random number    13
    ${c_regis_name}     BuiltIn.Set variable    REG${c_tax_id}
    ${c_name_th}        BuiltIn.Set variable    บริษัท ${c_regis_name} จำกัด
    ${c_name_en}        BuiltIn.Set variable    Company ${c_regis_name} Ltd
    ${c_sap_code}       BuiltIn.Set variable    REG${c_tax_id}
    ${c_phone}          common.Generate random number    8
    ${customer_items}   customer_api.Post create customer    ${login_cp_ticket_dev}
    ...    ${c_tax_id}    ${c_regis_name}    ${c_name_th}    ${c_name_en}
    ...    ${c_sap_code}    ${status_start_date}    ${c_phone}
    ${site_items}       Post Create Site    ${login_cp_ticket_dev}
    ...    ${customer_items}    ${c_regis_name}    Site-${c_regis_name}
    ...    ${status_start_date}    ${c_phone}
    ${s_tax_id}         common.Generate random number    13
    ${s_regis_name}     BuiltIn.Set variable    REG${s_tax_id}
    ${s_name_th}        BuiltIn.Set variable    บริษัท ${s_regis_name} จำกัด
    ${s_name_en}        BuiltIn.Set variable    Company ${s_regis_name} Ltd
    ${s_sap_code}       BuiltIn.Set variable    REG${s_tax_id}
    ${s_phone}          common.Generate random number    8
    ${s_bank_no}        common.Generate random number    10
    ${sub_contractor_id}    subcontractor_api.Post create sub contractor    ${login_cp_ticket_dev}    ${s_tax_id}    ${s_regis_name}    ${s_name_th}    ${s_name_en}    ${s_phone}    ${s_sap_code}    ${status_start_date}    ${s_bank_no}    ${s_name_th}
    ${s_id}             subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}    ${sub_contractor_id}
    ${random_string}    common.Generate random text    5    LETTERS
    ${dr_phone}         common.Generate random number    9
    ${dr_id}            common.Generate random number    13
    ${dr_license_id}    common.Generate random number    16
    ${driver_name}      BuiltIn.Set variable    REG${random_string}
    ${driver_id}        driver_api.Post create driver    ${login_cp_ticket_dev}    ${driver_name}    0${dr_phone}    ${dr_id}    ${dr_license_id}    ${status_start_date}    ${s_id}
    ${s_id}             subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}    ${sub_contractor_id}
    ${random_string}    common.Generate random text    2    LETTERS
    ${random_number}    common.Generate random number    4
    ${license_no}       BuiltIn.Set variable    8${random_string}${random_number}
    vehicle_api.Post create vehicle    ${login_cp_ticket_dev}    ${license_no}     ${driver_id}    ${s_id}    ${site_items}    ${c_regis_name}
    ${site_info_id}     site_api.Get site info ID    ${login_cp_ticket_dev}    Site-${c_regis_name}
    shipping_order_api.Post create confirm shipping order    ${login_cp_ticket_dev}
    ...    ${c_regis_name}    ${customer_items}    ${c_phone}
    ...    Site-${c_regis_name}    ${c_regis_name}    ${site_info_id}
    ${pid}    deliver_plan_api.Get PID for create dispatch order    ${login_cp_ticket_dev}    ${c_regis_name}
    ${create_route_id}    Create Dispatch Order    ${login_cp_ticket_dev}    ${pid}
    ${s_id}   subcontractor_api.Get sub contractor detail    ${login_cp_ticket_dev}    ${sub_contractor_id}
    Confirm Dispatch Order    ${login_cp_ticket_dev}    ${create_route_id}
    schedule_api.Assign driver and confirm dispatch    ${login_cp_ticket_dev}    ${create_route_id}    ${s_regis_name}
    ...    ${driver_name} Test    0${dr_phone}    ${license_no}    ${dr_license_id}

Check Activity Tracking
    ${login_cp_ticket_dev}    Post Login To TMS Web    ${users.tms_user}    ${users.tms_password}
    ${shipment_id}      BuiltIn.Set variable    14228
    ${job_id}           BuiltIn.Set variable    9645
    ${job_list_status}  Get list job tracking status    ${login_cp_ticket_dev}    ${job_id}
    Check tracking status by index      ${job_list_status}    1    JOB_HANDED_OVER_RECEIVER
    ${shipment_list_status}     Get list shippment tracking status    ${login_cp_ticket_dev}    ${shipment_id}
    Check tracking status by index      ${shipment_list_status}    1    SHIPMENT_UNLOADING
    ${order_id}         Get order id from shipment id    ${login_cp_ticket_dev}    ${shipment_id}
    ${order_list_status}        Get list order tracking status    ${login_cp_ticket_dev}    ${order_id}
    ${current_order_status}     Get current order status    ${login_cp_ticket_dev}    ${order_id}
    Check tracking status by index      ${order_list_status}    2    ORDER_PICKING_UP
    Check latest status                 ${current_order_status}    ORDER_DELIVERED