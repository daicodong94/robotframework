#!/usr/bin/env python

"""Usage: check_test_times.py seconds inpath [outpath]

Reads test execution result from an output XML file and checks that no test
took longer than given amount of seconds to execute.

Optional `outpath` specifies where to write processed results. If not given,
results are written over the original file.
"""

import sys
import requests
import time
import os
import json
from datetime import datetime, timedelta
from robot.api import ExecutionResult, ResultVisitor
jenkins_build_url = os.environ.get('BUILD_URL')
jenkins_build_number = os.environ.get('BUILD_NUMBER')
jenkins_branch = os.environ.get('BRANCH')
jenkins_GIT_BRANCH = os.environ.get('GIT_BRANCH')
network = os.environ.get('NETWORK_NAME')
elastic_ip = "125.26.15.143"
elastic_search_url = "http://" + elastic_ip +":13131/index"
today = datetime.today()
today_timestamp = datetime.strftime(today, '%Y%m%d %H:%M:%S.%f')
test_statistic = {
"job" : "Sample",
"count" : 1,
"test_count" : 0,
"pass_count" : 0,
"fail_count" : 0,
"duration_ms" : 0,
"run_result": 1,
"@timestamp": 0,   
"override_run_result" : 0,
"override_pass_count" : 0,
"override_reason" : "",
"reason_group_env" : 0,
"reason_group_bug" : 0,
"reason_group_script" : 0,
"build_duration_ms" : 0,
"updated_timestamp": 0,
"build_number": 0,  
"build_url": "",
"updated_timestamp": 0,
"stability_type": "relax",
"timestamp_2": 0,
"branch": ""
}

class ResultParser(ResultVisitor):
    def __init__(self):
        pass

    def visit_test(self, test):
        global test_statistic
        test_statistic['test_count'] = test_statistic['test_count']+1
        if test.status == 'PASS':
            test_statistic['pass_count'] = test_statistic['pass_count'] + 1
        elif test.status == 'FAIL':
            test_statistic['fail_count'] = test_statistic['fail_count'] + 1
            test_statistic['run_result'] = 0
        test_statistic['duration_ms'] = test_statistic['duration_ms'] + test.elapsedtime

def set_build_url():
    build_url = "None"
    if jenkins_build_url is not None:
        build_url = jenkins_build_url.replace("localhost","doppio-tech.com")
    test_statistic['build_url'] = build_url

def parse_result(job):
    result = ExecutionResult('./result/output.xml')
    result.visit(ResultParser())
    test_statistic['job'] = job
    test_statistic['build_number'] = jenkins_build_number
    starttime = get_first_start(result.suite)
    endtime = get_last_end(result.suite)
    started = datetime.strptime(starttime, '%Y%m%d %H:%M:%S.%f') if starttime is not None else datetime.strptime(today_timestamp, '%Y%m%d %H:%M:%S.%f')
    finished = datetime.strptime(endtime, '%Y%m%d %H:%M:%S.%f') if endtime is not None else datetime.strptime(today_timestamp, '%Y%m%d %H:%M:%S.%f')
    duration_ms = round((finished - started).total_seconds() * 1000)
    test_statistic['duration_ms'] = duration_ms
    set_build_url()
    insert_es(test_statistic)

def insert_es(test_stat):
    test_statistic['@timestamp'] = round(time.time() * 1000)
    test_statistic['timestamp_2'] = round(time.time() * 1000)
    print(test_statistic)
    print(elastic_search_url)
    resp = requests.post(url=elastic_search_url, json=test_statistic)
    print(resp.text)

def get_first_start(root, start=None):
    first_start_time = start
    for suite in root.suites:
        if suite.starttime is None: # branch node
            child_value = get_first_start(suite, first_start_time)
            first_start_time = child_value if start is None else min(first_start_time, child_value)
        else: # leaf node
            first_start_time = suite.starttime if first_start_time is None else min(suite.starttime, first_start_time)
            #print(f'Comparing {suite.starttime} with {first_start_time}')
    return first_start_time

def get_last_end(root, end=None):
    last_end_time = end
    for suite in root.suites:
        if suite.endtime is None: # branch node
            child_value = get_last_end(suite, last_end_time)
            last_end_time = child_value if end is None else max(last_end_time, child_value)
        else: # leaf node
            last_end_time = suite.endtime if last_end_time is None else max(suite.endtime, last_end_time)
    return last_end_time

if __name__ == '__main__':
    parse_result(*sys.argv[1:])
