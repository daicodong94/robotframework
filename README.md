# TMS Website automation

Automation for TMS website using robotframework

### Installation
## Clone Project
``` 
git clone https://{gitlab_user}@gitlab.com/asc-jiuye/automate/tmsb_automation_robotframework.git
```
## Required

- NodeJs
- Java SDK
- Python 3.x
- Appium 2
- chromedriver

Setup Guideline :
```
https://ascendcommerce.atlassian.net/wiki/spaces/COMQA/pages/3466330127/Mobile+Automation+with+Robotframework-Appium+Android
```

## Library
``` 
pip / pip3 install -r requirements.txt
``` 
### Install Homebrew (for mac user)
/bin/bash -c “$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)”
#### Install chromedriver
Type command: `brew install chromedriver`

## Run Test for Mobile App

1. Start Appium
```
appium --base-path /wd/hub
```
2. Start Emulator
   
3. Run Tests
#### Example Web

    robot -d results -v env:staging -v LANG:en -i web testcases/web/regression

### Quality code check
### Dry run command for Web

    robot --dryrun -v env:staging -v LANG:en testcases/web/regression