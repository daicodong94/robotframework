from    os          import  path
from    datetime    import  datetime
import  os
import  json
import  time
import  sys
import  base64
import  requests
from datetime import datetime
import zipfile

print("STARTING SENDER....")

jenkins_data = {
    "jenkins_build_url" : os.environ.get('BUILD_URL') if os.environ.get('BUILD_URL') else "",
    "jenkins_build_id" : os.environ.get('BUILD_NUMBER') if os.environ.get('BUILD_NUMBER') else 1,
    "jenkins_branch" : os.environ.get('BRANCH') if os.environ.get('BRANCH') else "",
    "jenkins_GIT_BRANCH" : os.environ.get('GIT_BRANCH') if os.environ.get('GIT_BRANCH') else "",
    "jenkins_project_id" : os.environ.get('PROJECT_ID') if os.environ.get('PROJECT_ID') else "",
    "jenkins_company_id" : os.environ.get('COMPANY_ID') if os.environ.get('COMPANY_ID') else "",
    "jenkins_automated_version" : os.environ.get('AUTOMATED_VERSION') if os.environ.get('AUTOMATED_VERSION') else "",
    "jenkins_app_version" : os.environ.get('APP_VERSION') if os.environ.get('APP_VERSION') else ""
}

type = os.environ.get('REPORT_TYPE') if os.environ.get('REPORT_TYPE') else ""
job = os.environ.get('JOB_NAME') if os.environ.get('JOB_NAME') else ""
build_type = os.environ.get('BUILD_TYPE') if os.environ.get('BUILD_TYPE') else ""
report_path = os.environ.get('REPORT_SEVER_PATH') if os.environ.get('REPORT_SEVER_PATH') else ""

with zipfile.ZipFile("result.zip", 'w', zipfile.ZIP_DEFLATED) as zip_file:
    if os.path.isfile(report_path):
        zip_file.write(report_path, os.path.basename(report_path))

def send_report():
    
    hostname = os.getenv("HOST_NAME")
    files={'report': open('result.zip','rb',)}
    jenkins_data["job"] = job
    jenkins_data["type"] = type
    jenkins_data["build_type"] = build_type
    jenkins_data["report_filename"] = os.path.basename(report_path)
    company_id=str(jenkins_data["jenkins_company_id"])
    project_id=str(jenkins_data["jenkins_project_id"])
    print(jenkins_data)
    endpoint = f"{hostname}/v1/companies/{company_id}/projects/{project_id}/builds"
    print(endpoint)
    resp = requests.post(
        endpoint,
        files=files,
        data=jenkins_data)
    print(resp.status_code)
    print(resp)
    return resp.status_code

if __name__ == '__main__':
    send_report()