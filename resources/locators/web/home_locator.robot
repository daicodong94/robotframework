*** Variables ***
${home.lbl_home_page}           xpath=//a[contains(@href,'dashboard')]
${home.img_change_language}     xpath=//div[contains(@class,'el-dropdown-selfdefine')]
${home.btn_th_lang}             xpath=//li[contains(@class,'el-dropdown-menu__item') and (contains(text(),'${btn_change_lang['th_lang']}'))]
${home.btn_en_lang}             xpath=//li[contains(@class,'el-dropdown-menu__item') and (contains(text(),'${btn_change_lang['en_lang']}'))]