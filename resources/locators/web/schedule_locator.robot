*** Variables ***
${schedule.lbl_dispatch_number}        xpath=//input[contains(@placeholder,'${schedule_page['lbl_dispatch_number']}')]
${schedule.txt_dispatch_searching}     xpath=//span[contains(@class,'search-highlight') and (contains(text(),'***string***'))]
${schedule.btn_inquire}                xpath=//span[contains(text(),'${schedule_page['btn_inquire']}')]
${schedule.btn_add_new_order}          xpath=//span[contains(text(),'${schedule_page['btn_add_new_order']}')]
${schedule.lbl_waybill_number}         xpath=//input[contains(@placeholder,'${schedule_page['lbl_waybill_number']}')]
${schedule.lbl_add_new_order}          xpath=//ul[contains(@class,'el-scrollbar__view el-autocomplete-suggestion__list')]//li[contains(text(),'***string***')]