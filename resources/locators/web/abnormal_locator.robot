*** Variables ***
${abnormal.txt_order_no}                       xpath=//input[contains(@placeholder,'${abnormal['txt_order_no']}')]
${abnormal.btn_inquire}                        xpath=//span[contains(text(),'${abnormal['btn_inquire']}')]
${abnormal.lbl_number_column}                  xpath=//div[contains(@class,'el-table__body-wrapper')]/table/tbody/tr
${abnormal.lbl_column_exception_type}          xpath=//div[contains(@class,'el-table__body-wrapper')]/table/tbody/tr[***index***]/td[6]/div
${abnormal.lbl_column_abnormal_status}         xpath=//div[contains(@class,'el-table__body-wrapper')]/table/tbody/tr[***index***]/td[4]/div
${abnormal.lbl_column_source_status}           xpath=//div[contains(@class,'el-table__body-wrapper')]/table/tbody/tr[***index***]/td[9]/div