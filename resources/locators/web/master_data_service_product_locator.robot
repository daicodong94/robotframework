*** Variables ***
${btn_service_product_action}            xpath=//span[text()='{8}']/parent::button
${btn_new}                               xpath=//span[normalize-space()='${service_product_page.btn_new}']
${txt_service_product_name}              xpath=//input[@data-vv-name='${service_product_page.lbl_name}']
${txt_service_product_code}              xpath=//input[@data-vv-name='${service_product_page.lbl_code}']
${ddl_click_attributes_list}             xpath=//td[@class='press']/div[@class='el-select common-input-style el-tooltip common-input-style el-select--mini input-init']
${ddl_select_service_product_attributes}       xpath=(//span[contains(text(),'${service_product_page['ddl_select_attributes_list']}')])[2]
${txt_service_short_name}                xpath=//input[@data-vv-name='${service_product_page.lbl_short_name}']
${txt_service_ratio}                     xpath=//input[@data-vv-name='${service_product_page.lbl_service_ratio}']
${btn_save}                              xpath=//button[@class='el-button common-button-style el-button--primary el-button--mini']
${btn_delete}                            xpath=//div[@class='el-table__fixed-right']//span[contains(normalize-space(),'${service_product_page['btn_delete']}')][1]
${btn_confirm}                           xpath=//span[contains(normalize-space(),'${service_product_page['btn_confirm']}')]