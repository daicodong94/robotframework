*** Variables ***
${txt_search}                   xpath=//input[@id="input-search"]
${btn_search}                   xpath=//button[@id="btn-search"]
${customer_search_result}       xpath=//p[contains(@class,'el-tooltip') and (contains(text(),'${TC_master_data_customer['customer_id']}'))]
${btn_customer_filter}          xpath=(//button[@class="el-button button-st el-button--default el-button--mini"])[2]
${ddl_customer_type}            xpath=//i[@class="el-select__caret el-input__icon el-icon-arrow-up"]
${ddl_customer_type_select}     xpath=//span[contains(text(),'***string***')]
${btn_customer_reset_filter}    xpath=//span[contains(text(),'${master_data['customer_reset_filter']}')]
${customer_filter_result}       xpath=//p[contains(@class,'el-tooltip') and (contains(text(),'${master_data['customer_personal']}'))]
${btn_customer_edit}            xpath=//svg[@class="cursor-pointer"]/div