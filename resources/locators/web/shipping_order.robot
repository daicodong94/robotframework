*** Variables ***
${btn_shipping_order_action}            xpath=//span[text()='***btn***']/parent::button
${ddl_ship_order_customer_name}         xpath=//div[@data-vv-name="Customer Name"]
${txt_search_keyword}                   xpath=//div[contains(@class,'search-candidate') and not(contains(@style,'none'))]/div[@class="search-cadidate-keyword"]/input
${li_candidate_item}                    xpath=//div[contains(@class,'search-candidate') and not(contains(@style,'none'))]/div[contains(@class,'search-candidate-list')]/ul/li[{0}]
${ddl_ship_order_origin_site_name}      xpath=(//input[@placeholder="Site Code/Name"])[1]
${ddl_ship_order_destination_site_name}    xpath=(//input[@placeholder="Site Code/Name"])[2]
${ddl_destination_type}                 xpath=//input[@placeholder="Please enter destination type"]
${li_list_item}                     xpath=//span[text()='{0}']/parent::li
${ddl_business_unit}                xpath=//input[@placeholder="Please enter business unit"]
${ddl_temperature_type}             xpath=//input[@placeholder="Please enter temperature type"]
${ddl_item_type}                    xpath=//input[@placeholder="Please select item type"]
${txt_total_number_package}         xpath=//input[@data-vv-name="Total Number of Packages"]
${txt_total_volumn}                 xpath=//input[@data-vv-name="Total Volume (cubic meter)"]
${txt_total_weight}                 xpath=//input[@data-vv-name="Total Weight (kg)"]
${txt_total_item_per+package}       xpath=//input[@data-vv-name="Total Number of Items per Packages"]
${ddl_target_pickup_time}           xpath=//tr[td[text()='${shipping_order['txt_target_date']}']]//following-sibling::input[@placeholder="${shipping_order['lbl_select_date']}"]
${ddl_target_deliver_time}          xpath=//tr[td[text()='${shipping_order['txt_delivery_time']}']]//following-sibling::input[@placeholder="${shipping_order['lbl_select_date']}"]
${txt_pickup_delivery_date}         xpath=(//input[@placeholder="${shipping_order['txt_enter_date']}"])[2]
${btn_date_ok}                      xpath=(//button[text()='Ok'])[2]
${btn_pick_date_ok}                 xpath=(//button[text()="${revise_page['btn_pickadate_ok']}"])[2]
${btn_deliver_date_ok}              xpath=(//button[text()="${revise_page['btn_pickadate_ok']}"])[2]
${ddl_revise_customer_name}         xpath=//div[@data-vv-name="partnerName"]
${ddl_revise_origin_site}           xpath=(//input[contains(@placeholder,'${revise_page['lbl_site_code_name']}')])[1]
${ddl_revise_destination_site}      xpath=(//input[contains(@placeholder,'${revise_page['lbl_site_code_name']}')])[2]
${txt_revise_target_pickup_date}    xpath=(//div[contains(@class,"time-date-range-input")])[1]
${txt_revise_target_deliver_date}   xpath=(//div[contains(@class,"time-date-range-input")])[2]
${txt_revise_pickup_date}           xpath=//input[@placeholder="${revise_page['lbl_target_pickup']}"]
${lnk_more_filter}                  xpath=//span[text()='${shipping_order['btn_more_filter']}']/parent::button
${txt_filter_shipping_order}        xpath=//div[@class='mt5']//input[contains(@placeholder,'please enter the transport order')]
${btn_inquire}                      xpath=//span[text()='${shipping_order['btn_inquire']}']/parent::span/parent::button
${chk_select_shipping_order}        xpath=//table[@class='el-table__body']/tbody/tr[{0}]/td[1]//label
${btn_confirm_revise}               xpath=(//span[text()='${shipping_order['btn_revise']}']/parent::button)[2]
${txt_customer_name}                xpath=//input[@placeholder="${shipping_order['txt_customer_name']}"]
${txt_filter}          xpath=(//div[@class="search-cadidate-keyword"]/input)[last()]
${tr_shipping_order_row}            xpath=//table[@class='el-table__body']/tbody/tr
${lbl_td_customer_sap_code}         xpath=//div[text()='Customer Name']
${lbl_td_order_no}                  xpath=//table[@class='el-table__body']/tbody/tr[{0}]/td[3]/div
${lbl_td_order_status}              xpath=//table[@class='el-table__body']/tbody/tr[{0}]/td[4]/div
${txt_filter_status}                xpath=//div[@class='mt5']//input[@placeholder='${shipping_order['sel_order_status']}']
${lbl_li_status}                    xpath=//li[contains(@class,'el-select-dropdown__item')]/span[text()='{0}']
${txt_shipping_order_no}            xpath=//input[contains(@placeholder,'${shipping_order['lbl_shipping_order_number']}') and not(contains(@icon,'search'))]
${ddl_customer_sap_code}            xpath=//input[contains(@placeholder,'${shipping_order['lbl_customer_sap_code']}') and not(contains(@icon,'search'))]
${lbl_customer_sap_code}            xpath=//span[@class='partner-shortname' and text()='***sap_code***']
${lbl_number_column}                xpath=(//div[@class="el-table__header-wrapper"])[1]//th
${lbl_column_name}                  xpath=(//div[@class="el-table__header-wrapper"])[1]//th[***index***]/div
${lbl_txt_from_column}              xpath=//div[@class="el-table__body-wrapper is-scrolling-left"][1]//td[***index***]/div
${txt_old_shipping_order_number}    xpath=//input[contains(@placeholder,'***old_shipping_order_no***') and not(contains(@icon,'search'))]
${btn_delete_shipping_order_number}     xpath=//i[@class="el-select__caret el-input__icon el-icon-circle-close"]
${btn_collapse_filter}              xpath=//span[contains(text(),'${shipping_order['btn_collapse_filter']}')]
${lbl_scheduling_page}                xpath=//div[@id="tab-CARRIER" and text()='${shipping_order['lbl_scheduling_page']}']
