*** Variables ***
${common.li_main_menu}             xpath=//ul[@role='menubar']//span[normalize-space()='{0}']
${common.li_sub_menu}              xpath=//ul[@role='menu']//span[normalize-space()='{0}']
${common.lbl_success_message}      xpath=//div[contains(@class,'el-message--success')]/p[text()='{0}']
${common.dialog_popup}             xpath=//div[@class="el-message-box__message"]/p
${common.btn_dialog_confirm}       xpath=(//span[normalize-space()='${shipping_order['btn_confirm']}']/parent::button)[2]
${common.btn_confirm}              xpath=(//span[contains(text(),'${shipping_order['btn_confirm']}')])[last()]
${common.btn_confirm_capital}      xpath=//span[text()='${shipping_order['btn_confirm_capital']}']