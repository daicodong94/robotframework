*** Variables ***
${txt_login_email}          xpath=//input[@name="email"]
${txt_login_password}       xpath=//input[@name="password"]
${btn_login}                xpath=//span[text()='Login']/parent::div/parent::button
${btn_remember_me}          xpath=//label[text()='${login['btn_remember_me']}']
