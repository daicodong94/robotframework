*** Settings ***
Library    String
Library    OperatingSystem
Library    Collections
Library    DateTime
Library    SeleniumLibrary    run_on_failure=SeleniumLibrary.Capture Page Screenshot
Library    RequestsLibrary
Library    JSONLibrary
Library    ExcelLibrary.py
Library    AppiumLibrary    run_on_failure=AppiumLibrary.Capture Page Screenshot
Library    DebugLibrary
Library    DatabaseLibrary

Resource    ${CURDIR}/../keywords/common.robot
Resource    ${CURDIR}/../keywords/mobile/common.robot
Resource    ${CURDIR}/../keywords/web/common.robot

# Features
Resource    ${CURDIR}/../keywords/mobile/feature/example_feature.robot
Resource    ${CURDIR}/../keywords/api/feature/order_api_feature.robot
Resource    ${CURDIR}/../keywords/api/feature/master_data_api_feature.robot
Resource    ${CURDIR}/../keywords/api/feature/activity_tracking_api_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/login_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/shipping_order_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/delivery_plan_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/home_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/master_data_service_product_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/abnormal_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/customer_list_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/vehicle_list_feature.robot
Resource    ${CURDIR}/../keywords/web/feature/schedule_feature.robot

# Api
Resource    ${CURDIR}/../keywords/api/login_api.robot
Resource    ${CURDIR}/../keywords/api/notification_api.robot
Resource    ${CURDIR}/../keywords/api/order_status_api.robot
Resource    ${CURDIR}/../keywords/api/common.robot
Resource    ${CURDIR}/../keywords/api/site_api.robot
Resource    ${CURDIR}/../keywords/api/customer_api.robot
Resource    ${CURDIR}/../keywords/api/driver_api.robot
Resource    ${CURDIR}/../keywords/api/subcontractor_api.robot
Resource    ${CURDIR}/../keywords/api/vehicle_api.robot
Resource    ${CURDIR}/../keywords/api/shipping_order_api.robot
Resource    ${CURDIR}/../keywords/api/deliver_plan_api.robot
Resource    ${CURDIR}/../keywords/api/schedule_api.robot
Resource    ${CURDIR}/../keywords/api/activity_tracking_api.robot

# Pages
Resource    ${CURDIR}/../keywords/mobile/pages/example_page.robot
Resource    ${CURDIR}/../keywords/web/pages/web_login_page.robot
Resource    ${CURDIR}/../keywords/web/pages/shipping_order_list_page.robot
Resource    ${CURDIR}/../keywords/web/pages/shipping_order_create_page.robot
Resource    ${CURDIR}/../keywords/web/pages/home_page.robot
Resource    ${CURDIR}/../keywords/web/pages/delivery_plan_page.robot
Resource    ${CURDIR}/../keywords/web/pages/master_data_service_product_page.robot
Resource    ${CURDIR}/../keywords/web/pages/abnormal_page.robot
Resource    ${CURDIR}/../keywords/web/pages/customer_list_page.robot
Resource    ${CURDIR}/../keywords/web/pages/vehicle_list_page.robot
Resource    ${CURDIR}/../keywords/web/pages/schedule_page.robot

# Locators
Resource    ${CURDIR}/../resources/locators/mobile/example_locator.robot
Resource    ${CURDIR}/../resources/locators/web/login_locator.robot
Resource    ${CURDIR}/../resources/locators/web/common_locator.robot
Resource    ${CURDIR}/../resources/locators/web/shipping_order.robot
Resource    ${CURDIR}/../resources/locators/web/home_locator.robot
Resource    ${CURDIR}/../resources/locators/web/delivery_plan_locator.robot
Resource    ${CURDIR}/../resources/locators/web/master_data_service_product_locator.robot
Resource    ${CURDIR}/../resources/locators/web/abnormal_locator.robot
Resource    ${CURDIR}/../resources/locators/web/customer_list_locator.robot
Resource    ${CURDIR}/../resources/locators/web/vehicle_list_locator.robot
Resource    ${CURDIR}/../resources/locators/web/schedule_locator.robot

# Database
Resource    ${CURDIR}/../keywords/database/database.robot

# Configs
Variables   ${CURDIR}/configs/${env}/env_config.yaml
Variables   ${CURDIR}/configs/${env}/config.yaml

# Test data
Variables   ${CURDIR}/testdata/web/${env}/test_data_web.yaml
Variables   ${CURDIR}/testdata/mobile/${env}/test_data_mobile.yaml
Variables   ${CURDIR}/testdata/translation/web_translation/translation_${LANG}.yaml
Variables   ${CURDIR}/testdata/translation/mobile_translation/translation_${LANG}_mobile.yaml